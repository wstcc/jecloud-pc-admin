## [2.2.3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.2.2...v2.2.3) (2024-02-23)


### Bug Fixes

* **build:** 修改编辑器配置 ([492e4f6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/492e4f6eeae3db3e97eeb6ea3048d6a171fd7eed))
* **je:** 增加useAdmin函数的属性[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([313abe3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/313abe3be02f9afd115d86b15800974bf036dd40))
* **login:** 修复登录接口加密后，导致参数异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6a784e1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6a784e1e7a3ba2d56683060317a5b228e291e2d3))


### Features

* **version:** release v2.2.1 ([923781d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/923781d54fefa65301512e28a76eadde893d110b))
* **version:** release v2.2.2 ([73fa96f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/73fa96fa9c11489edc4a48494558464b68cdb1e3))



## [2.2.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.2.1...v2.2.2) (2023-12-29)



## [2.2.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.2.0...v2.2.1) (2023-12-15)


### Bug Fixes

* **public:** 优化静态资源文件[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([9d1cedd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/9d1cedd12b451b0e12a4858aec932b3dfde0eea9))
* **router:** 修复功能路由参数失效问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([8a1b2aa](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/8a1b2aaf2c6418e754d34a6a6926bd19289d87e7))


### Features

* **admin:** 逻辑加判空提交 ([443b24a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/443b24aee9ad5b38bdc9f9352d3427ba03162454))
* **flie:** 添加图片预览资源 ([9cb8d34](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/9cb8d3471501c37c9182aeaca06bda2ea021f5db))
* **version:** release v2.1.0 ([f5ca7b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f5ca7b9f44666d6509850aae42dbae53e9a9567f))
* **version:** release v2.1.1 ([6dc31ab](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6dc31abb52c2ee4d69ba9e6b6d8bc1783de0b4a7))
* **version:** release v2.2.0 ([8d1a7a8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/8d1a7a8a8a4a3ccff04f887a890cb4868483680c))



# [2.2.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.1.1...v2.2.0) (2023-11-15)



## [2.1.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.1.0...v2.1.1) (2023-10-27)



# [2.1.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.0.9...v2.1.0) (2023-10-20)


### Bug Fixes

* **http:** 优化http请求的拦截器和退出登录的逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([61178f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/61178f33af0d27bbd0a7f1798e6e6eba8209810c))
* **third:** 三方登录接口放开token校验[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([e04ead9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e04ead9c282060124ba8c3b81f4f5b1aa12a38d3))
* **token:** 优化token失效后直接退出登录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([bedb8d3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/bedb8d321bb011309a8ec0a36fb38b81e7c6b4df))


### Features

* **version:** release v2.0.8 ([8970821](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/89708211d43becbdbff332f64005cf60c108b366))
* **version:** release v2.0.9 ([0fbfd4a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0fbfd4add77a87758df961c9e95ef2e3687a307f))



## [2.0.9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.0.8...v2.0.9) (2023-09-22)


### Bug Fixes

* **micro:** 修复微应用加载机制[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([988dc34](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/988dc34c800aa61c975d43c36fd28a91721b45c2))



## [2.0.8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.0.7...v2.0.8) (2023-09-15)


### Bug Fixes

* **env:** 更新服务代理地址[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([25bafcb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/25bafcb0e894b9a126d1b7067e8ab7edf2e81cc5))


### Features

* **doc:** 文件changelog修改提交 ([0028b1a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0028b1a8b43fc25201838cb86d94951dba5fe1ac))
* **micro:** 微应用支持动态注入指定dom元素[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([ad1fc24](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/ad1fc24f3c99efe459e892b27bfc1aea29d606d6))
* **script:** 增加setup:lib命令，提供非源码客户使用[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a2911c9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a2911c9b761941d3af6f7e73b5e7fb91f8aff203))
* **sourcecode:** 客户源码定版v1.0.0 ([4ce7891](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4ce7891d9a83c806ef58fabed739e9a95895d1e1))
* **version:** release v2.0.4 ([abfadad](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/abfadad80aac3a9f264dff1c2c861d9dbcc0ffc8))
* **version:** release v2.0.5 ([14f06f4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/14f06f42d697b2fcd4132d72e9348059593347d5))
* **version:** release v2.0.6 ([75f479d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/75f479d97a391a8e43078bbae15e0d82c21b69a5))
* **version:** release v2.0.6 ([0e84c35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0e84c358a52e1ca7df1040f58dfad18348faaa56))
* **version:** release v2.0.7 ([ca2a996](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/ca2a9961629a05cd7e9f4e8978e3446a558ce3e3))



## [2.0.7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.0.6...v2.0.7) (2023-09-08)



## [2.0.6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.0.5...v2.0.6) (2023-09-01)


### Features

* **admin:** 切换部门不展示数据修复 ([8c636e0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/8c636e02e9ad6ebd83b74a75a845d907642eed76))



## [2.0.5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.0.4...v2.0.5) (2023-08-25)



## [2.0.4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v2.0.3...v2.0.4) (2023-08-18)



## [2.0.3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v1.4.0...v2.0.3) (2023-08-12)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 修复websocket代理地址失效问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6975273](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/69752730079a6686f1be71e01f454f4f24f07609))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **config:** 调整env配置文件，增加websocket配置信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([ca48a64](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/ca48a642908545e4c29e618c8c0d059d9ec408e0))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **func:** 废弃功能配置绑定，交由libs项目处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([66b5831](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/66b5831e9e1d183778d6750016a3356b2d70d4cd))
* **license:** 调整授权代码目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([de2db35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/de2db354b02e54679f2464f7454f94f0c664e7bc))
* **license:** 修复证书校验逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f8e8220](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f8e822097f206ca9091dda831dd73b5577097eef))
* **menu:** 顶部菜单 ，当只有一个时，不显示[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2f6dc76](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2f6dc762348fe8a673745b580e82c242da17ee94))
* **menu:** 顶部菜单点击后左侧定位到功能目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0dffca5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0dffca54716f26a2930900c55aa26902a73aa76a))
* **menu:** 通过子应用打开菜单，需要进行所属方案处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([8b132c0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/8b132c091c83bb7d100efd9bdabbd8865901248b))
* **menu:** 修复菜单恢复逻辑问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0e467f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0e467f3c19ad40d0868f4b49213d81f08973a5f1))
* **menu:** 修复恢复菜单记录问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d8e0cb2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8e0cb2fddc29410a2cb6a6f6b5a079e2fbd2d26))
* **micro:** 调整微应用编码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0d83fb0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0d83fb098a39b1625974fd7f5d175b3a449d1c19))
* **micro:** 调整微应用菜单打开方式，可以根据微应用配置动态打开[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a4e5b9c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a4e5b9c4e9c84705b8ae5bb66eba8a3e36cf6d98))
* **micro:** 调整微应用事件触发逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5dc785f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/5dc785fe9e6bd1b7abbbde491dcd4f498779628e))
* **micro:** 动态加载微应用，增加判空处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([60a98f7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/60a98f71e1c9091b4084bb4b378ab43ed786d3fb))
* **micro:** 加载微应用地址自动追加location.pathname[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([116e775](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/116e775a94262e130dee0ef5cf92dc9f1d1ab79f))
* **micro:** 删除废弃代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d5c2ee6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d5c2ee6df3474cc041b546929002b99d7b869689))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **public:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2175446](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21754467d4ddf676b3fc79a1026665ae43f39396))
* **search:** 菜单全局搜索增加高亮[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a1a972a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a1a972a984c260eca18efb6c8fe0a753a68bf6c0))
* **style:** 调整部分选中背景色[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([40c0628](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/40c0628a66b95e84e81a6f405a483a4e9400d76a))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 共享系统数据，防止子应用再次请求[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([e15405a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e15405a35f8b0a9acce832326de6ae59001a65c4))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 调整主题样式，适配新的主题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4b15c5e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4b15c5e4bfa2df54eb12adb79ff9843cfefae2dd))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **third:** 修复三方登录token校验规则[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([52b2cb0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/52b2cb06ed358626e2a4b67ef7a11df87516a3f5))
* **update:** 提交消息声音[[#12](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/12)] ([7f5cf48](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/7f5cf4802760dc0d41d85f8b5e879a1650cea4e7))


### Features

* **admin:** 给所有微应用绑定主应用信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f9ad34a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f9ad34af3deab08734b1903379abeee5513f580d))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6b4dacf](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6b4dacfd8136df403b6ee0e8cc79d621dca80bd0))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2dc199d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2dc199dd91d400e5623c7674adafb346341badb5))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **license:** 增加系统技术支持，license文件[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59dea73](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59dea7300631a998f80c3b4cea11ddedc7c0fc64))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **micro:** 增加JE.useAdmin()函数，暴露更多的微应用的函数处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d00a27e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d00a27e3d138662d5a06d5a576710aefb16313af))
* **search:** 增加全局菜单搜索[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2ca30eb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2ca30ebdef24d9eb653075e2b041327426d10f98))
* **security:** 增加密级管理的适配[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6ca7f78](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6ca7f7878871b7aff46adc10b12acb21fa05c628))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **theme:** 增加系统前端变量JE_SYSTEM_THEME_SETTINGS，控制禁用主题设置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([21b0729](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21b07299606217c146ae8e6448e0c67462d639af))
* **third:** 增加三方登录token检查checkThirdToken[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([3e0ce0c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/3e0ce0cbe7194267017b1270bbf5ffe0318f103b))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([26b1885](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/26b1885719cca06e6d9f1a60f3c236018079e791))
* **version:** release v2.0.0 ([cbcc35d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/cbcc35d8d385ab594e5eb2410db4475440d88154))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([05b26c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/05b26c2177d0ee287d75dff3b334d14348e4a9e3))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))
* **version:** release v2.0.3 ([1793e6b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/1793e6b39724654d98563036fade36e4282d8b14))
* **version:** release/2.0.2 ([77e8043](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/77e8043bd9825fdd7fa7943cdcfd20129a9ea510))



## [2.0.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v1.4.0...v2.0.2) (2023-08-04)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **config:** 调整env配置文件，增加websocket配置信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([ca48a64](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/ca48a642908545e4c29e618c8c0d059d9ec408e0))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **func:** 废弃功能配置绑定，交由libs项目处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([66b5831](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/66b5831e9e1d183778d6750016a3356b2d70d4cd))
* **license:** 调整授权代码目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([de2db35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/de2db354b02e54679f2464f7454f94f0c664e7bc))
* **license:** 修复证书校验逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f8e8220](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f8e822097f206ca9091dda831dd73b5577097eef))
* **menu:** 顶部菜单 ，当只有一个时，不显示[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2f6dc76](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2f6dc762348fe8a673745b580e82c242da17ee94))
* **menu:** 顶部菜单点击后左侧定位到功能目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0dffca5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0dffca54716f26a2930900c55aa26902a73aa76a))
* **menu:** 通过子应用打开菜单，需要进行所属方案处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([8b132c0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/8b132c091c83bb7d100efd9bdabbd8865901248b))
* **menu:** 修复菜单恢复逻辑问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0e467f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0e467f3c19ad40d0868f4b49213d81f08973a5f1))
* **menu:** 修复恢复菜单记录问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d8e0cb2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8e0cb2fddc29410a2cb6a6f6b5a079e2fbd2d26))
* **micro:** 调整微应用编码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0d83fb0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0d83fb098a39b1625974fd7f5d175b3a449d1c19))
* **micro:** 调整微应用菜单打开方式，可以根据微应用配置动态打开[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a4e5b9c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a4e5b9c4e9c84705b8ae5bb66eba8a3e36cf6d98))
* **micro:** 调整微应用事件触发逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5dc785f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/5dc785fe9e6bd1b7abbbde491dcd4f498779628e))
* **micro:** 动态加载微应用，增加判空处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([60a98f7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/60a98f71e1c9091b4084bb4b378ab43ed786d3fb))
* **micro:** 加载微应用地址自动追加location.pathname[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([116e775](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/116e775a94262e130dee0ef5cf92dc9f1d1ab79f))
* **micro:** 删除废弃代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d5c2ee6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d5c2ee6df3474cc041b546929002b99d7b869689))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **public:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2175446](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21754467d4ddf676b3fc79a1026665ae43f39396))
* **search:** 菜单全局搜索增加高亮[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a1a972a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a1a972a984c260eca18efb6c8fe0a753a68bf6c0))
* **style:** 调整部分选中背景色[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([40c0628](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/40c0628a66b95e84e81a6f405a483a4e9400d76a))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 共享系统数据，防止子应用再次请求[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([e15405a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e15405a35f8b0a9acce832326de6ae59001a65c4))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 调整主题样式，适配新的主题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4b15c5e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4b15c5e4bfa2df54eb12adb79ff9843cfefae2dd))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **third:** 修复三方登录token校验规则[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([52b2cb0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/52b2cb06ed358626e2a4b67ef7a11df87516a3f5))
* **update:** 提交消息声音[[#12](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/12)] ([7f5cf48](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/7f5cf4802760dc0d41d85f8b5e879a1650cea4e7))


### Features

* **admin:** 给所有微应用绑定主应用信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f9ad34a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f9ad34af3deab08734b1903379abeee5513f580d))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6b4dacf](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6b4dacfd8136df403b6ee0e8cc79d621dca80bd0))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2dc199d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2dc199dd91d400e5623c7674adafb346341badb5))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **license:** 增加系统技术支持，license文件[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59dea73](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59dea7300631a998f80c3b4cea11ddedc7c0fc64))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **micro:** 增加JE.useAdmin()函数，暴露更多的微应用的函数处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d00a27e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d00a27e3d138662d5a06d5a576710aefb16313af))
* **search:** 增加全局菜单搜索[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2ca30eb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2ca30ebdef24d9eb653075e2b041327426d10f98))
* **security:** 增加密级管理的适配[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6ca7f78](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6ca7f7878871b7aff46adc10b12acb21fa05c628))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **theme:** 增加系统前端变量JE_SYSTEM_THEME_SETTINGS，控制禁用主题设置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([21b0729](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21b07299606217c146ae8e6448e0c67462d639af))
* **third:** 增加三方登录token检查checkThirdToken[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([3e0ce0c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/3e0ce0cbe7194267017b1270bbf5ffe0318f103b))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([26b1885](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/26b1885719cca06e6d9f1a60f3c236018079e791))
* **version:** release v2.0.0 ([cbcc35d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/cbcc35d8d385ab594e5eb2410db4475440d88154))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([05b26c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/05b26c2177d0ee287d75dff3b334d14348e4a9e3))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))



## [2.0.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v1.4.0...v2.0.1) (2023-07-30)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **config:** 调整env配置文件，增加websocket配置信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([ca48a64](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/ca48a642908545e4c29e618c8c0d059d9ec408e0))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **func:** 废弃功能配置绑定，交由libs项目处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([66b5831](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/66b5831e9e1d183778d6750016a3356b2d70d4cd))
* **license:** 调整授权代码目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([de2db35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/de2db354b02e54679f2464f7454f94f0c664e7bc))
* **license:** 修复证书校验逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f8e8220](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f8e822097f206ca9091dda831dd73b5577097eef))
* **menu:** 顶部菜单 ，当只有一个时，不显示[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2f6dc76](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2f6dc762348fe8a673745b580e82c242da17ee94))
* **menu:** 顶部菜单点击后左侧定位到功能目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0dffca5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0dffca54716f26a2930900c55aa26902a73aa76a))
* **menu:** 通过子应用打开菜单，需要进行所属方案处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([8b132c0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/8b132c091c83bb7d100efd9bdabbd8865901248b))
* **menu:** 修复菜单恢复逻辑问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0e467f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0e467f3c19ad40d0868f4b49213d81f08973a5f1))
* **menu:** 修复恢复菜单记录问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d8e0cb2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8e0cb2fddc29410a2cb6a6f6b5a079e2fbd2d26))
* **micro:** 调整微应用编码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0d83fb0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0d83fb098a39b1625974fd7f5d175b3a449d1c19))
* **micro:** 调整微应用菜单打开方式，可以根据微应用配置动态打开[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a4e5b9c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a4e5b9c4e9c84705b8ae5bb66eba8a3e36cf6d98))
* **micro:** 调整微应用事件触发逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5dc785f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/5dc785fe9e6bd1b7abbbde491dcd4f498779628e))
* **micro:** 动态加载微应用，增加判空处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([60a98f7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/60a98f71e1c9091b4084bb4b378ab43ed786d3fb))
* **micro:** 加载微应用地址自动追加location.pathname[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([116e775](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/116e775a94262e130dee0ef5cf92dc9f1d1ab79f))
* **micro:** 删除废弃代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d5c2ee6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d5c2ee6df3474cc041b546929002b99d7b869689))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **public:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2175446](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21754467d4ddf676b3fc79a1026665ae43f39396))
* **search:** 菜单全局搜索增加高亮[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a1a972a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a1a972a984c260eca18efb6c8fe0a753a68bf6c0))
* **style:** 调整部分选中背景色[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([40c0628](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/40c0628a66b95e84e81a6f405a483a4e9400d76a))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 共享系统数据，防止子应用再次请求[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([e15405a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e15405a35f8b0a9acce832326de6ae59001a65c4))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 调整主题样式，适配新的主题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4b15c5e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4b15c5e4bfa2df54eb12adb79ff9843cfefae2dd))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **third:** 修复三方登录token校验规则[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([52b2cb0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/52b2cb06ed358626e2a4b67ef7a11df87516a3f5))
* **update:** 提交消息声音[[#12](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/12)] ([7f5cf48](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/7f5cf4802760dc0d41d85f8b5e879a1650cea4e7))


### Features

* **admin:** 给所有微应用绑定主应用信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f9ad34a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f9ad34af3deab08734b1903379abeee5513f580d))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6b4dacf](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6b4dacfd8136df403b6ee0e8cc79d621dca80bd0))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2dc199d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2dc199dd91d400e5623c7674adafb346341badb5))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **license:** 增加系统技术支持，license文件[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59dea73](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59dea7300631a998f80c3b4cea11ddedc7c0fc64))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **micro:** 增加JE.useAdmin()函数，暴露更多的微应用的函数处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d00a27e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d00a27e3d138662d5a06d5a576710aefb16313af))
* **search:** 增加全局菜单搜索[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2ca30eb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2ca30ebdef24d9eb653075e2b041327426d10f98))
* **security:** 增加密级管理的适配[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6ca7f78](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6ca7f7878871b7aff46adc10b12acb21fa05c628))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **theme:** 增加系统前端变量JE_SYSTEM_THEME_SETTINGS，控制禁用主题设置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([21b0729](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21b07299606217c146ae8e6448e0c67462d639af))
* **third:** 增加三方登录token检查checkThirdToken[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([3e0ce0c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/3e0ce0cbe7194267017b1270bbf5ffe0318f103b))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([26b1885](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/26b1885719cca06e6d9f1a60f3c236018079e791))
* **version:** release v2.0.0 ([cbcc35d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/cbcc35d8d385ab594e5eb2410db4475440d88154))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d9b316b2671fb760e1456907c216adff855d4537))



# [2.0.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/compare/v1.4.0...v2.0.0) (2023-07-22)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **config:** 调整env配置文件，增加websocket配置信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([ca48a64](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/ca48a642908545e4c29e618c8c0d059d9ec408e0))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **func:** 废弃功能配置绑定，交由libs项目处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([66b5831](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/66b5831e9e1d183778d6750016a3356b2d70d4cd))
* **license:** 调整授权代码目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([de2db35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/de2db354b02e54679f2464f7454f94f0c664e7bc))
* **license:** 修复证书校验逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f8e8220](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f8e822097f206ca9091dda831dd73b5577097eef))
* **menu:** 顶部菜单 ，当只有一个时，不显示[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2f6dc76](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2f6dc762348fe8a673745b580e82c242da17ee94))
* **menu:** 顶部菜单点击后左侧定位到功能目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0dffca5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0dffca54716f26a2930900c55aa26902a73aa76a))
* **menu:** 通过子应用打开菜单，需要进行所属方案处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([8b132c0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/8b132c091c83bb7d100efd9bdabbd8865901248b))
* **menu:** 修复菜单恢复逻辑问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0e467f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0e467f3c19ad40d0868f4b49213d81f08973a5f1))
* **menu:** 修复恢复菜单记录问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d8e0cb2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8e0cb2fddc29410a2cb6a6f6b5a079e2fbd2d26))
* **micro:** 调整微应用编码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([0d83fb0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/0d83fb098a39b1625974fd7f5d175b3a449d1c19))
* **micro:** 调整微应用菜单打开方式，可以根据微应用配置动态打开[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a4e5b9c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a4e5b9c4e9c84705b8ae5bb66eba8a3e36cf6d98))
* **micro:** 调整微应用事件触发逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5dc785f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/5dc785fe9e6bd1b7abbbde491dcd4f498779628e))
* **micro:** 动态加载微应用，增加判空处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([60a98f7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/60a98f71e1c9091b4084bb4b378ab43ed786d3fb))
* **micro:** 加载微应用地址自动追加location.pathname[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([116e775](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/116e775a94262e130dee0ef5cf92dc9f1d1ab79f))
* **micro:** 删除废弃代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d5c2ee6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d5c2ee6df3474cc041b546929002b99d7b869689))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **public:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2175446](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21754467d4ddf676b3fc79a1026665ae43f39396))
* **search:** 菜单全局搜索增加高亮[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a1a972a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a1a972a984c260eca18efb6c8fe0a753a68bf6c0))
* **style:** 调整部分选中背景色[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([40c0628](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/40c0628a66b95e84e81a6f405a483a4e9400d76a))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 共享系统数据，防止子应用再次请求[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([e15405a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e15405a35f8b0a9acce832326de6ae59001a65c4))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 调整主题样式，适配新的主题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([4b15c5e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/4b15c5e4bfa2df54eb12adb79ff9843cfefae2dd))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **update:** 提交消息声音[[#12](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/12)] ([7f5cf48](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/7f5cf4802760dc0d41d85f8b5e879a1650cea4e7))


### Features

* **admin:** 给所有微应用绑定主应用信息[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f9ad34a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f9ad34af3deab08734b1903379abeee5513f580d))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6b4dacf](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6b4dacfd8136df403b6ee0e8cc79d621dca80bd0))
* **admin:** 增加JE.useAdmin().openMenu函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2dc199d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2dc199dd91d400e5623c7674adafb346341badb5))
* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **license:** 增加系统技术支持，license文件[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59dea73](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59dea7300631a998f80c3b4cea11ddedc7c0fc64))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **micro:** 增加JE.useAdmin()函数，暴露更多的微应用的函数处理[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([d00a27e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/d00a27e3d138662d5a06d5a576710aefb16313af))
* **search:** 增加全局菜单搜索[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([2ca30eb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2ca30ebdef24d9eb653075e2b041327426d10f98))
* **security:** 增加密级管理的适配[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([6ca7f78](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/6ca7f7878871b7aff46adc10b12acb21fa05c628))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **theme:** 增加系统前端变量JE_SYSTEM_THEME_SETTINGS，控制禁用主题设置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/issues/8)] ([21b0729](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/21b07299606217c146ae8e6448e0c67462d639af))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([26b1885](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/26b1885719cca06e6d9f1a60f3c236018079e791))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-admin/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/compare/v1.4.0...v1.4.1) (2023-06-30)


### Bug Fixes

* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **func:** 废弃功能配置绑定，交由libs项目处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([66b5831](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/66b5831e9e1d183778d6750016a3356b2d70d4cd))
* **menu:** 顶部菜单 ，当只有一个时，不显示[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2f6dc76](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2f6dc762348fe8a673745b580e82c242da17ee94))
* **menu:** 顶部菜单点击后左侧定位到功能目录[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0dffca5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0dffca54716f26a2930900c55aa26902a73aa76a))
* **menu:** 通过子应用打开菜单，需要进行所属方案处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([8b132c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8b132c091c83bb7d100efd9bdabbd8865901248b))
* **menu:** 修复菜单恢复逻辑问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0e467f3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0e467f3c19ad40d0868f4b49213d81f08973a5f1))
* **menu:** 修复恢复菜单记录问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([d8e0cb2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d8e0cb2fddc29410a2cb6a6f6b5a079e2fbd2d26))
* **micro:** 调整微应用事件触发逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([5dc785f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5dc785fe9e6bd1b7abbbde491dcd4f498779628e))
* **micro:** 动态加载微应用，增加判空处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([60a98f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/60a98f71e1c9091b4084bb4b378ab43ed786d3fb))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **search:** 菜单全局搜索增加高亮[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([a1a972a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a1a972a984c260eca18efb6c8fe0a753a68bf6c0))
* **system:** 共享系统数据，防止子应用再次请求[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e15405a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e15405a35f8b0a9acce832326de6ae59001a65c4))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **update:** 提交消息声音[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/12)] ([7f5cf48](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7f5cf4802760dc0d41d85f8b5e879a1650cea4e7))


### Features

* **admin:** 给所有微应用绑定主应用信息[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f9ad34a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f9ad34af3deab08734b1903379abeee5513f580d))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **micro:** 增加JE.useAdmin()函数，暴露更多的微应用的函数处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([d00a27e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d00a27e3d138662d5a06d5a576710aefb16313af))
* **search:** 增加全局菜单搜索[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2ca30eb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2ca30ebdef24d9eb653075e2b041327426d10f98))
* **security:** 增加密级管理的适配[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([6ca7f78](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6ca7f7878871b7aff46adc10b12acb21fa05c628))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))



# [1.4.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/compare/v1.2.0...v1.4.0) (2023-06-06)


### Bug Fixes

* **ajax:** 修复主子应用共用ajax实例[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([94f40c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/94f40c21f97664e3f0150830f1e6e93ed1849aa9))
* **file:** 适配uploadFile参数 ([00057b7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/00057b719b9d31bbe2170f26097ce8407d5d02d6))
* **func:** 优化字典功能配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([d49aae2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d49aae29292fc05d7fae9ed8999c8f1aa842eabc))
* **header:** 修复切换部门错误问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([6bef634](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6bef6343be97d9100de7de76d7abdf0dd19ff2a0))
* **index:** 删除系统默认图标和标题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([5484e67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5484e67ecea8b08abefb91d8987626f25cefa439))
* **layout:** 调整系统页面整体布局结构[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([fbee206](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fbee2063129d4bb127e2fcc9bf8b5f2165e17e39))
* **main:** 调整应用入口依赖引入[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([3424113](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3424113eb908c1f3bdfafe175194037d3ef72191))
* **menu:** 点击顶部菜单时，左侧多级菜单，默认不展开[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([dadfe0d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dadfe0d0377ca1fb852c5de3e4b58c52cb4ce8da))
* **menu:** 调整菜单缺省页的样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e50c209](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e50c209627330e7940166908c9643f6b027c9e1b))
* **menu:** 记录菜单打开状态逻辑优化[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([d099599](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d0995993b7594a909743c8bed79a4a40ce84da8f))
* **menu:** 适配顶部绑定重复菜单[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([b34f546](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b34f54654c5629b1ee55a9139c468a9e4142b4cf))
* **menu:** 修复顶部菜单居中显示问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([fb2f328](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fb2f3285178aa93a218456a59feff14fe824cd79))
* **menu:** 优化记录功能的逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0bfad3b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0bfad3bc7a532ff1b35f65ad9be8939102b31411))
* **micro:** 修复加载方案和微应用接口不需要token校验[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([af7cb68](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/af7cb6844a84e152c61f61f54499749a130e9025))
* **micro:** 修复微应用组件加载采用路由地址作为索引[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([74b988a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/74b988acf2d7af59e0de07260d8354deeee2458f))
* **plugin:** 废弃@jecloud/plugin,调整JE的注册[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([052a918](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/052a918eec2e9cdab556e52fbecc727384ebecd4))
* **public:** 修复公共资源打包错误问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e1138f8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e1138f8bcd284c84e7705644fe133e232451418d))
* **router:** 修复路由守卫处理方案参数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([9dd6953](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9dd69534e529aba23d7a96da5527aceebb23f34b))
* **store:** 将src/data/store调整为src/stores，增加微应用默认编码枚举[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e6c509a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e6c509afaafd0f1ce8e22d21ad0c4230d995c2fe))
* **third:** 配合三方登录将系统路由守卫整合[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([6be5d8a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6be5d8a9b7e394370e84844685fa0923baae38e1))
* **third:** 三方登录登录接口调整[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([d088c39](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d088c392344f2ebb0af37aeb883eb19f0e23814a))
* **third:** 三方登录支持参数传递 ([94e577d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/94e577df4d6b0962f5db0838184a20446930e71c))
* **third:** 修复三方登录逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([815763d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/815763dfee962557be06e5754e6107cadfdd8bf7))
* **utils:** @jecloud/utils删除vue依赖[[#67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/67)] ([e6929e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e6929e0d0df43b58c15f3bdd41d20d525f30fa20))
* **utils:** 适配jecloud/utils的调整 ([47b8cdd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/47b8cdd79cfd1d2a459f809a5ebbf8bcd11ce8e5))
* **utils:** 适配jecloud/utils函数调整[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([da88781](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/da88781fa0be3bfbef451761e5093a40d55d19df))


### Features

* **admin:** 打开菜单暴露的方法参数修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([47cf390](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/47cf390736b344677a21d6e047bfd47dddb52bbf))
* **admin:** 个人中心出生日期数据格式转换[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([932c983](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/932c983ffb7e6dab5ff052a7e507d8f8c06c9d13))
* **admin:** 更改文件预览路由配置[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([61d2448](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/61d2448ff28ccb63d1f0e69734edc37e8d9f504c))
* **admin:** 解析图表菜单[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([0eb2bbb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0eb2bbba1ec2ced0d3780575dc46422fdd12b61f))
* **admin:** 路由白名单增加文件预览[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([a78f156](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a78f156815420adad1d0cad71d1c7694299506e2))
* **admin:** 通知提示音修复[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([19f24f9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/19f24f93fe58406602afa83f2c98d04e94d40810))
* **admin:** 图片预览区分节点加载[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([d753261](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d753261d49d7c186f8740aa26ea5c988b6e0d916))
* **admin:** 文件预览路由配置增加白名单配置[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([3ae86cc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3ae86cc90001fd4e919736d845e4f97d597171d2))
* **admin:** 文件预览增加当前正在预览的文件信息[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([b84576f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b84576fcce2c00a91a131f47c295882688df7167))
* **admin:** 修改文件预览白名单名称[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([67bc53b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/67bc53b760ef1f34122d2357e2f47d2eb1efe24c))
* **admin:** 增加文件预览显示状态[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([2d49e2a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2d49e2af787205bc2e1a5b577103b317ffe6d135))
* **admin:** 增加文件预览新窗口打开路由及配置文件[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([29af23d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/29af23d5e071acfa3d4766674b8fcd6074eec7be))
* **admin:** 增加文件整合静态资源[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([673fa28](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/673fa285c911f8a899def9d0211c378bba0d27b2))
* **admin:** iframe支持参数解析[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([6b199a0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6b199a0fb8026fbc8e25c786561e69d8e4b06bfe))
* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 图片预览区分节点加载[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/11)] ([2876a2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2876a2eed3a5e13047094d4034aff39e88bebfcf))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **bi:** 门户引擎，图表引擎采用微应用方式打开[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([4e7a35b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4e7a35b4f020dc8535bec0a11ec0998e8776ff8e))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/affc489c088b3b77dbd2d176a887562726735540))
* **func:** 增加表单加载默认数据支持 ([2e52248](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2e522488a43e5762b274223e5ecac3bba6b3efd9))
* **je:** 暴露JE常用方法 ([4efb75b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4efb75b4881eae4000153638e1d0d4b4628547cb))
* **je:** 将vue的h函数添加到JE上 ([753fad9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/753fad92b4fbf4a7aabbcc29fdd3b950ad3f426a))
* **menu:** 增加菜单缺省页[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([551958b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/551958b9e514a208ed2c4d210d4f856dbadc14d5))
* **menu:** 增加记录历史菜单功能[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([7cac3b0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7cac3b09b1eef776f771e7400881c59db96d29ed))
* **menu:** 增加记录用户操作菜单激活tab[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([6d3fb45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6d3fb45d181a40357181bcf151d43aed9f54de43))
* **micro:** 微应用支持query参数接收 ([f0ca765](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f0ca765ef26210993cead813c8f57efbfa3ffa98))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **third:** 增加三方登录适配 ([a851f0b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a851f0bb795ac0824785c3862e48817d4a8a399c))
* **version:** release v1.3.0 ([c31b79f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c31b79f5c3ed275327b826954d62362026b8e09b))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.3.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/compare/v1.2.0...v1.3.0) (2023-05-05)


### Features

* **admin:** 打开菜单暴露的方法参数修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([47cf390](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/47cf390736b344677a21d6e047bfd47dddb52bbf))
* **admin:** 解析图表菜单[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([0eb2bbb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0eb2bbba1ec2ced0d3780575dc46422fdd12b61f))
* **admin:** 路由白名单增加文件预览[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([a78f156](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a78f156815420adad1d0cad71d1c7694299506e2))
* **admin:** 文件预览增加当前正在预览的文件信息[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([b84576f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b84576fcce2c00a91a131f47c295882688df7167))
* **admin:** 修改文件预览白名单名称[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([67bc53b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/67bc53b760ef1f34122d2357e2f47d2eb1efe24c))
* **admin:** 增加文件预览显示状态[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([2d49e2a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2d49e2af787205bc2e1a5b577103b317ffe6d135))
* **admin:** 增加文件预览新窗口打开路由及配置文件[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([29af23d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/29af23d5e071acfa3d4766674b8fcd6074eec7be))
* **admin:** 增加文件整合静态资源[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([673fa28](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/673fa285c911f8a899def9d0211c378bba0d27b2))
* **admin:** iframe支持参数解析[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([6b199a0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6b199a0fb8026fbc8e25c786561e69d8e4b06bfe))
* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/affc489c088b3b77dbd2d176a887562726735540))
* **func:** 增加表单加载默认数据支持 ([2e52248](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2e522488a43e5762b274223e5ecac3bba6b3efd9))
* **micro:** 微应用支持query参数接收 ([f0ca765](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f0ca765ef26210993cead813c8f57efbfa3ffa98))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.2.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/compare/v1.1.0...v1.2.0) (2023-04-06)


### Bug Fixes

* **build:** 修复样式文件采用参数引用方式 ([dc6fd3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dc6fd3fecc0c02b536590915888f967333cdce44))
* **copyright:** 简化插件检查逻辑 ([fbe193d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fbe193da4fef5be9fd8491c3227fd3bf1215c1cd))
* **copyright:** 简化插件检查逻辑 ([482660d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/482660d19806a759754b0739220daef8a3b77b6e))
* **copyright:** 增加前端变量PC_LICENCE ([2b8ec99](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2b8ec99681c45eb31592a54d710e4af1fe879ee0))
* **copyright:** 增加PC_LICENCE变量 ([226621d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/226621d6a08e73aa871ddd20d87549fb3fd43f13))
* **font:** 修复字体文件为版本参数引用 ([a353a02](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a353a02d0cf04447a92ae635604b4545a6a18f63))
* **photo:** 修复首页用户头像丢失问题 ([8a93847](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8a9384748f6405a5943e0f3bed88db97a58f3dba))
* **websocket:** 修复websocket链接前缀 ([d86b9fb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d86b9fbfddfc2b0a4963a39480da8948bc12e972))


### Features

* **admin:** 插件加载时添加loading样式 ([feb142f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/feb142fa1a992b92967ef63fbf39952e2e6674cd))
* **cli:** changelog提交 ([0a4c001](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0a4c001c14276bacd26ff9dd3b5798fab203a459))
* **cli:** packagelock文件提交 ([9282afd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9282afd0bc17b871511e3d59bf7f9f2fee378440))
* **cli:** v1.1.0定版 ([11af62b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/11af62bd47230ee6d422effa56e4dd446b0ecca0))
* **pdf:** pdf预览资源 ([530c87e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/530c87e6eea990882732446b3ca3a8f74310968a))



# [1.1.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/compare/v1.0.2...v1.1.0) (2023-03-03)


### Bug Fixes

* **code:** 调整代码，适应所有分支结构 ([2e7d448](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2e7d44879680fda6cf2af72b7c6b1fd4cc18ed5c))
* **i18n:** 修改欢迎页i18n文字，变量导致火狐52卡死[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([ae5b45c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ae5b45c45bc02fb81f0e80df6ad1b84686f0e7c4))
* **icon:** 升级图标配置 ([ae975c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ae975c4dec20dcb889971157953f7227826c56ab))
* **logout:** 修复退出登录接口[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/9)] ([e244604](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e244604c6aa1bfd01268e4cbf35af8e7a82afc44))
* **logout:** 增加退出登录接口调用[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/9)] ([bda04a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bda04a21469159c1973ab121b15c49c81f838425))
* **menu:** 修复app路由下标题展示错误问题[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/5)] ([6446044](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/64460441bfb5b8bee77af179e515606e13488681))
* **npm:** 废弃pnpm安装，采用原始npm安装依赖[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([9e653ec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9e653ec168b9b0ce44aafb2f59c656ba1f2811cd))
* **npm:** 更新npm lock文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([11cbdc5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/11cbdc56dafbc7f0ada60ac8b0bf8716840ef493))
* **npm:** 修改npm私服地址 ([cb61aad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/cb61aadb4077424573b8b7e8770972d0091aa569))
* **package:** 删除qiankun依赖[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/5)] ([fa551ce](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fa551cefb4f12afa99f4a1c2da5d0c74fcd746ac))
* **websocket:** 修复websocket地址支持读取配置参数[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([0425791](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/042579184595d8320af48da35d9b18bf48073c6f))


### Features

* **定版:** 1.0.1定版 ([df7a773](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/df7a773d4691ccb5cf5cbf77c339058d70b7375c))
* **定版:** 1.01定版 ([8f03fe1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8f03fe15e8dd6d34ef029848a86bb0d7aa3d0931))
* **更新依赖:** 更新依赖 ([1fdf852](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1fdf852f4a25cc79dfcdb48effc7423074a2ed65))
* **admin:** 把预览markdow添加到路由白名单[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([121bd14](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/121bd14f78095fbba7407ad604f00754c67ca0fe))
* **admin:** 菜单加载逻辑修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([e0f1eac](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e0f1eac75dea808f31ce143d71805c057a059d79))
* **admin:** 顶部菜单支持iframe和url[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([ab7016e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ab7016e7f5223c00ecfdf08355aad1ee6cfc1295))
* **admin:** 更新json文件[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([0fb6498](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0fb6498485d68c0da834543b1dca109efd8eea3c))
* **admin:** 接口markdown预览路由修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([c442601](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c44260197975c65f9ca40bb4cc0caa44a83ef48a))
* **admin:** 添加预览markdown路由以及页面[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([7597a45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7597a4505dbe990144b620e952b45bd106fd9853))
* **admin:** ajax代码回退 ([bd89e98](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bd89e9888ad1927b2f252a5fb718a032a67d838a))
* **admin:** ajax请求token修改 ([9245cb6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9245cb66dcf2af8f82fbbdbe7e0683598248fb30))
* **admin:** changelog提交 ([a670c5b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a670c5bbe56d5e742768dc72cd03b4f73e08f696))
* **admin:** v1.1.0定版 ([f871788](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f8717886959745d10e9e8bbe4a59aa03dfd6072d))
* **admin:** WebSocket加入白名单路由 ([87fbb4a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/87fbb4a48f5f169b9f90d82e14bdb0066f3e28dc))
* **admin:** yaml文件更新 ([b16cb15](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b16cb156dc269ac249d81b65a04e491ce90902ef))
* **cli:** package-lock文件提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([9f37092](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9f370921f4dd15533377b8ec432d149bc5500ef8))
* **func:** 支持单功能链接访问[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/5)] ([81a6051](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/81a60513d3faa5247acfa7130213767f1a4ab97b))
* **func:** 支持功能表单展示模式[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/5)] ([cc83da2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/cc83da29bf43dc8075cdaaa34f4876b70a396c6b))
* **init:** 增加初始化加载系统配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([a2820a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a2820a46173411a049263d9358473f79f6d6e64d))
* **locales:** 国际化方法修改[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/10)] ([ef3aef4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ef3aef469c19a2a90ae28339a6ecd48175e80557))
* **locales:** 国际化方法修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([66d70f8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/66d70f8aa34922201eece1ba31168edb6e8e68c8))
* **md:** 火狐52浏览器适配文档提交 ([6b5c00b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6b5c00b76bcff13164ce0f79364c1f06f7afc882))
* **md:** 火狐52浏览器适配文档提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/10)] ([75c0262](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/75c0262173c62362ad83ef5be7dfef578ae159de))
* **menu:** iframe类型菜单修改[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([bb56ff8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bb56ff8d27bde6c540e1fc563fe2f03bd8d5e22e))
* **route:** 增加token认证路由[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/5)] ([01cc5e7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/01cc5e7230530273adb94b95d3014a86555e85f6))



# [1.0.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/compare/28659bb098e260eee3fec6522782483d5defdf0b...v1.0.0) (2022-08-30)


### Bug Fixes

* **admin:** 去除微应用library配置，自动读取[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([fb68636](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fb6863671557ce8979af26a9b670dc8c67c6f7db))
* **admin:** 删除调试代码[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([735da0b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/735da0bcdc1915ff004706d1866ffb22d371e4f8))
* **admin:** 修复功能路由展示页面[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([7a482f1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7a482f1e495156be0ebef4828872b17d95a054e4))
* **admin:** 增加应用布局[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([998a81f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/998a81fc845b467538c7159f2b081c074dfe8778))
* **admin:** 主应用qiakun与业务代码隔离 ([b2e28c9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b2e28c968c167f5df2f8b4a765ada05c58231f20))
* **admin:** 主子应用打通 ([7afb056](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7afb0562c91be650cd3fb38fb79e15868bc2506c))
* **ant:** 更新ant-design-vue@~3.1.0-rc ([4ca2bd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4ca2bd307e68b797e96d8a9269cdde02ba795435))
* **antd:** 调整vite依赖引用 ([74942d5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/74942d5abe0b9579966d859f542fc6bb9c264d81))
* **antd:** 暂注掉antd图标按需加载，后期调试 ([c5b4766](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c5b476687c631a14dee0540080843dd7a90092ec))
* **app:** 修复单独功能页，logo浅色主题反显色[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([612cf91](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/612cf9174da2cb358a0ab4c0f57e80de6e255e38))
* **assets:** 支持动态加载微应用资源 ([a121b57](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a121b57a0fbf949d3f4efc44d989dcc3a1e95449))
* **axios:** 修复axios格式化数据 ([145baff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/145baff06da0a57e5b765b6ec85c21246923afae))
* **axios:** 修复axios格式化数据 ([1c98fbf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1c98fbf1e59027e16ef328dee9746e6b1f19eb82))
* **axios:** 增加axios微应用代理前缀prexyPrefix[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([76b6650](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/76b6650d43b09fbcb130bb4941729dec562497b0))
* **babel:** 修改按需加载引用目录为es ([38ac102](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/38ac102a89be1dc747856862090935197065fdb9))
* **base:** 根据开发规范修改文件命名[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/1)] ([d03badf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d03badf4aea253280b40eaf16e7fcd88208a3299))
* **bug:** 修复主题，登录等bug ([9116e9b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9116e9bb7524f8fe1b2f25870694c2f930baa002))
* **build:** 调整项目打包配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([8a20fe0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8a20fe0fb4a0a3022dce708c5fe28ed9ebefbd5a))
* **build:** 去除sourceMap[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0f91481](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0f914813b0e422cd8a26fafb8c9cf006a9483907))
* **build:** 剔除monaco打包处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([01b92ac](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/01b92acff392264ed061de55542104569796cba7))
* **build:** 提供公共资源的json配置文件，方便应用调用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([3122798](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3122798d67192219d4cf2cc44b25c6f98d2845bd))
* **build:** 修复微应用打包后的数据文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([bc40d94](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bc40d94ba4547e7e047933cf44e9ecc042aac8a9))
* **build:** 修复env配置文件 ([0044821](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0044821ee5f3107e1998dbac970fdf46524a727b))
* **build:** 修改webpack打包配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([395061c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/395061c23f9612d753f930006957fa7c04e2ba98))
* **build:** 优化打包流程 ([3cf58a3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3cf58a361e80f2fb3f10d39c157bb26a9c6ec7e0))
* **build:** 增加图片资源打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([5ef32f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5ef32f6b8deadbc6a526daac23892e06366c82c7))
* **build:** 主子应用打包使用相同配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([63ed9d3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/63ed9d34fd536af080e46e41c8b8b37e10af05ee))
* **code:** 调整代码结构 ([3ef4c2d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3ef4c2d69dde2428574985b7c5b3c68fa312824e))
* **code:** 整体代码结构调整 ([684da4a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/684da4a18ea9d37911955e78c3a74427a3f205ac))
* **code:** 主应用代码修复 ([75c8972](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/75c89721bd68c0744edb20a8f817f281ed397454))
* **css:** 统一调整antd个性化样式引入方式 ([d7d1bbd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d7d1bbdc3166a0de1982d75029428adae67de525))
* **css:** 修复主题样式引用错误问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([5193877](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5193877901bfd64b892d080aa144e8e71415358c))
* **doc:** 由于pnpm7不兼容yalc，不支持file:xxx安装本地包，所以请使用 pnpm6[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([c461e36](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c461e36575353d9814b02e5360f08e1e562a3b25))
* **env:** 修改env配置文件配置[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([f2a6e39](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f2a6e396f33397f76d6ff2e07444198b4890184f))
* **envs:** 修改系统变量 ([015b6a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/015b6a21b59a90e0cd7f89c531edf8085b231d06))
* **eslint:** 增加 'no-case-declarations': 'warn'[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([08e2b46](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/08e2b46ade750da82458707642d4cb546e04c5db))
* **file:** 更新pnpm-lock文件 ([b7aed24](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b7aed246df4955c07883568d910d300a513aad67))
* **html:** 增加时间戳，防止html页面缓存 ([57d674a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/57d674aac11410f0efe0dcb4d8bfe90c5f2453eb))
* **i18n:** 抽离监听国际化的方法[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([c4506b4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c4506b49d16f8e17f4629e1ddeee15c80226d035))
* **i18n:** 调整国际化，增加使用说明 ([65947d9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/65947d92303588185715f8711fc58a56d4e96591))
* **i18n:** 调整i18n代码结构 ([26768ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/26768aee6fb6294c7462923af3a65c46b16c4c0d))
* **i18n:** 去除i18n打包配置，导致微应用异常[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0bd0cec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0bd0ceca303cbb87c460607bde6a96ad49b05ff1))
* **i18n:** 修复初始化系统采用中文[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([2e5262c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2e5262c2c8cbecacf8a068e2e5d51d6d8e4ed0f4))
* **i18n:** 修复国际化问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f502c95](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f502c95b34e4207dce9fb17316d41d880bde71c2))
* **i18n:** 修复国际化问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([352b873](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/352b8734fa951dfabec3e611812f80f5d7c2dac1))
* **i18n:** 修复i18n旧模式下api兼容 ([450d2cf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/450d2cf1a1f23ca02c85b871e2cf4436c25fafbd))
* **icon:** 更新图标文件 ([732a444](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/732a44439fce9a759abd67bb82ff8bbc56a73592))
* **icon:** 修改图标字段由iconCls统一改为icon[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([c4f056a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c4f056a703c68fe6cd0835e30fc4e846935cbe44))
* **image:** 删除无用文件 ([28659bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/28659bb098e260eee3fec6522782483d5defdf0b))
* **index:** 修复初始化配置设置 ([64d3c8e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/64d3c8e813b53b99213fe23ddd0b3e08c2fa807d))
* **index:** 修复打包错误 ([cf96bfa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/cf96bfa76a11751356fb44cea55b1edc412164d9))
* **je:** 调整je按需加载 ([6da9c79](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6da9c793339785e7f7dd7628224106fa0379e2ba))
* **je:** 修复主应用注入je方法问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([b5f3f0d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b5f3f0dd034019be8b0536f4f4bd3e0a6995ab06))
* **je:** 修复主应用注入je方法问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([a1cf61c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a1cf61c61bb0554d1579a91982d562cfcaa5aa37))
* **layout:** 修改路由容器不支持滚动条 ([46a5b04](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/46a5b0413141d68e659cd32c63fa2c95f444fe04))
* **less:** 固定less版本3.0.4,防止主题构建有问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([03b82b6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/03b82b6cbeec0603824c19f9e2c81d3b76541408))
* **libs:** 更新所有包的依赖[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([e937611](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e93761151741781f9655d0c07738ed9f19f4bbeb))
* **libs:** 更新libs项目[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e0df5f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e0df5f61d16a63e1b7ac29dfbf4f53b931991be0))
* **libs:** 更新libs项目[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f8809bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f8809bb47660553bad5a9e69c45f05c68c4b69da))
* **lodash:** 修复lodash工具包依赖 ([9dd17e2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9dd17e20446fa90b7503eed1fa4070c2b3d2c970))
* **lodash:** 增加lodash工具包 ([6c13199](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6c13199b0eae2484ba16d6fe4f1b6a5c6d246ad7))
* **login:** 抽离login方法，提供全局调用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([729a2c5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/729a2c548229491a6a5242fb2798fcb6945f19a0))
* **login:** 集成登录环境[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([c7f2926](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c7f29262bc80540df2851ebc4aee0be6653afd5c))
* **login:** 接入系统登录[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([083fc41](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/083fc41acf3cfceb59b2722e7176c1007792daf1))
* **login:** 微应用登录页丢失，默认使用系统登录[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([02b0b61](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/02b0b612009da3e6017b9f362bfd200e70a5891b))
* **login:** 微应用登录页丢失，默认使用系统登录[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([15735dd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/15735dd58155ee5441fcae9fc266693abc398714))
* **login:** 修复登录成功后，路由跳转问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([a66b4c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a66b4c0323c788c752ced8d837a83c891023b601))
* **login:** 修复登录后路由跳转问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2ea0c62](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2ea0c62add33877bdc995bb27d9623019904c870))
* **login:** 修复登录相关操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([4747fcd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4747fcd3b915c28606fc070649db627b0ed0953c))
* **login:** 修复用户失效，退出登录 ([2eed3ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2eed3ae3626937f7769284d7bf51897c8b41af1a))
* **login:** 修复主子应用交互混乱问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e42b4ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e42b4ffda48cd7afaaeb3bf3fb4f4a87331e8b14))
* **login:** 优化登录页同主应用保持一致[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([39587c9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/39587c989a192aebfa2d42d224fd4c9c9a19968c))
* **login:** 优化登录页同主应用保持一致[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([15cbd0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/15cbd0e5c1fea45fe97f874a11ab4ab733d628b1))
* **login:** 增加网站备案号 ([e42573f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e42573f02341aad3b0a6fdf84600194d8cddf8e6))
* **login:** 增加网站备案号 ([f8641f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f8641f71111a5cae697ca8398cd5f158e13f1142))
* **login:** 增加login样式 ([7a0db20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7a0db20513807519bfa04048074ffa3b639f1689))
* **logo:** 主题浅色系时，logo反色处理[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([d122b1b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d122b1b026e2705e113a2af767986b15308f80b9))
* **logout:** 将logout从JE抽离到system ([abf67e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/abf67e05098cc8f85c478294ca8de9c34a875dd0))
* **logout:** 系统退出，触发子应用admin-logout事件[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([4dee7f2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4dee7f26f98364793acdb993c3ede55cb9131f64))
* **main:** 修复入口文件引入[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/2)] ([f389efc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f389efcf12c4b90d8160140136f02b14602bd724))
* **menu:** 菜单所有数据交互完成[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([c227445](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c227445eee8e333d4b018e9b52a94f973a075483))
* **menu:** 导航菜单下拉菜单图标位置调整[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([b17ed8a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b17ed8ab1d38cfbfc8a5c2e9b852beff43398375))
* **menu:** 调整菜单路由 ([9dde6af](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9dde6af722fe888ab177ba251c4ac1007df1280b))
* **menu:** 调整顶部菜单过多时遮挡问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([1e3981a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1e3981a16015405c7ae23d4f21ee46eecf4f7faa))
* **menu:** 顶部菜单切换布局面板[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([55db1b8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/55db1b871dbfd589cf9f2f9a34e3ed1c56af375d))
* **menu:** 功能标签可以在新窗口中打开[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([823eb3a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/823eb3a8db9afe1f5a695804ff372c826a4ed675))
* **menu:** 兼容顶部菜单绑定功能模块失效问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([f63d0c5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f63d0c5e45b7831f2f620c4937c070602e6d3d28))
* **menu:** 去除首页菜单展开动画[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([8bc7da8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8bc7da8ca65249b05969292ee04f01c07d654700))
* **menu:** 首页菜单支持只打开一级父级[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([59d49d7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/59d49d72340b8fe28b65a355f2c55086afa3c134))
* **menu:** 修复菜单收起后，顶部导航消失问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([03893c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/03893c4bc43c995bc43de72b048b09a59a87ae04))
* **menu:** 修复菜单无效时报错[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([bcfe87a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bcfe87a89186220b96d928510bcccd029d0a7d40))
* **menu:** 修复打开菜单展示不正确问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([9dc699f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9dc699fd0b09c10d3b3e901a0634787547f9492f))
* **menu:** 修复单功能菜单也能记录历史[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([7f7de36](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7f7de36d1e6482b01e374a7a6c3ae719bbc223b0))
* **menu:** 修复登录成功后，打开默认首页菜单[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([620ffaa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/620ffaa82cfbe1d4586f8e48313049046bd768e7))
* **menu:** 修复顶部菜单操作逻辑[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([e5a2a72](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e5a2a72949e4c2c6ddadc731b6950054cf057360))
* **menu:** 修复顶部菜单打开权限[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([16dbdef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/16dbdefa811676c9778093d93485189ea13a8fcf))
* **menu:** 修复顶部菜单关联多菜单的问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([ffdfa0d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ffdfa0d8865075ed7b3963b85134bfb83294bcaa))
* **menu:** 修复顶部导航图标间距[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([ef44e4a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ef44e4afb9e3933fd642c58cc1c2a8bd06bfa005))
* **menu:** 修复自定义打开菜单展示错误[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([67e5fd8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/67e5fd8dff5c8c0b6f8c5461f2c6b2be72fb1a81))
* **menu:** 修复左侧菜单搜索失效问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([63b94d0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/63b94d0209cab23f4406cd8801df4af62082aac3))
* **menu:** 修改顶部菜单样式名称 ([1ba68ca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1ba68ca9db42e1b6561e14fac9f45e3ae16a7385))
* **menu:** 优化菜单打开方式[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([5da645c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5da645cc3e0b520e2ce55c450201af7066027fc6))
* **menu:** 增加菜单绑定失败输出日志[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([95b1da3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/95b1da385e27ed9468d361711f7ab29283f171f2))
* **menu:** 增加顶部菜单校验逻辑[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([011f28d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/011f28df61aa93baa1de5aa2788520cceac7ae61))
* **menu:** 增加图标库，展板菜单[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([28fdb88](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/28fdb88281ea48b02365243e75d72f21c99a539a))
* **menu:** 重构顶部菜单和左侧菜单[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([44e82fe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/44e82fe30197846a1d8d307db492b0208627a6c1))
* **menu:** 左侧菜单只允许打开一级[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([764d150](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/764d15091d528a74bfb8c2a867df14c9738cf440))
* **micro:** 抽离安装子应用的函数[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([d2a0191](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d2a0191f4b7cf6a6b25d714392b8774475ed56ca))
* **micro:** 调整微应用代码结构 ([91501cb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/91501cb6a6024d28583e712cf5da1e4d382c3f8f))
* **micro:** 将所有插件功能判定为微应用[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([09785e4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/09785e4114eadef7ca0124585700eb13b2e436da))
* **micro:** 去除微应用加载标记[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([638916a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/638916a690bdce31d00f13e12a0d83009522b433))
* **micro:** 删除server8的代理地址[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([802c217](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/802c217834927e41683187e516ff4621ebacd7d5))
* **micro:** 设置登录后的系统信息[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([00c52c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/00c52c2a947c7154353cee61d4b9be376c696bfa))
* **micro:** 设置登录后的系统信息[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([8facb67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8facb67d32b666828fc06998ce44380a6694e8e9))
* **micro:** 设置子应用共享主应用系统数据[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([ebc8432](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ebc843288f4d94d8946022ed7b2de4de07b5f283))
* **micro:** 统一微应用编码为：JE_CORE开头[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([2020549](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2020549031e73705a16f1ac602883e3c1f21d687))
* **micro:** 修复不同布局子应用加载失败问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([8f02d18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8f02d18803e9cd83f78af05cd3912a3eb4daba95))
* **micro:** 修复微应用引入axios错误[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([73f6c40](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/73f6c40803c5a48aff43defcb3ecf9149c9a468d))
* **micro:** 修复微应用store的bug[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([623ab1f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/623ab1fbe691db046400734493c161dfc402b558))
* **micro:** 修复子应用加载前，先销毁历史数据[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([cc7f785](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/cc7f785c5c23c5d5eee155ab14c10d5213bcd595))
* **micro:** 修复子应用加载失败，404[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([b7ea674](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b7ea6741b51edc9c5d5dce12fcb8d41a0243a87f))
* **micro:** 修复子应用注册登录成功事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f542360](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f542360d7f67dc2e3b3ad49f56b84dffd4b54fa4))
* **micro:** 修复microStorebug[[#45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/45)] ([16dda0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/16dda0effefd2b376e533ad44d9961ebcde43916))
* **micro:** 优化主子应用交互方式 ([e4b9be6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e4b9be68f5b458310b2f5db509a0ac65865c298b))
* **micro:** 优化子应用打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e3a93c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e3a93c248e3d672dc532c42dfc41c8ee04d21226))
* **micro:** 优化子应用静态资源请求链接，增加入口文件缓存标识[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([acaed06](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/acaed0636eb878898eb0774bf61bff376c741bea))
* **micro:** 原有初始配置插件改为普通插件[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([ae70cd7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ae70cd7615785948abbbb5cb1f2883269ae7df5e))
* **micro:** 增加微应用控制[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([bebda01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bebda019b930f198da1d354480a723a29efdf047))
* **micro:** 增加微应用控制负责度[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([7e9fd5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7e9fd5fa3f4091a2298d0c35bbb5a25eb4a59673))
* **micro:** 增加微应用store的name属性[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0ef5917](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0ef5917ed71601dcc559709e6b62ee90e33f44a4))
* **micro:** 增加主应用标识判断 ([a71ee5d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a71ee5df08d2017d5adf2c198a096ac021dde80c))
* **micro:** 增加子应用激活路由配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([44ea958](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/44ea958233db96c2810d3a0a46619a441c0e44e4))
* **micro:** 支持动态加载微应用[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([7e6d624](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7e6d624d7f9e8889197a84377ed065a33af6e2fa))
* **micro:** webpack打包主应用不处理output[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f3dc8b1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f3dc8b1dd5f3573fa4d4ce5efbb018ba42ca22c5))
* **mock:** 修复mock例子中的错误单词 ([c7f67f8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c7f67f8fc7f9de63b6e76d8e5a469a084643993a))
* **monaco:** 去除monaco打包插件，通过静态资源引用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([dddb641](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dddb641dd444d319df243ba5095be33057b54a3b))
* **mxgraph:** 修复源码，将所有属性注入到window，兼容沙箱模式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([44b6b3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/44b6b3ffcf8dfdf4a7df8389373d5b489795dbc5))
* **npm:** 更新npm包信息[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([0749e48](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0749e485ab35ac06a699649df98d3576184a3066))
* **npm:** 更新pnpm-lock文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([ab75426](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ab7542619055c2599fe514ae398d52248ebbdfe4))
* **package:** 更新所有包的依赖[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f60ecd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f60ecd323b10533ad62bf689dff0c7aeb7a31a9b))
* **package:** 更新ant-design-vue版本[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([89efa95](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/89efa95a824e42ecfb4cbc2203e6b53c2e32b24c))
* **package:** 更新libs包[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([5f7f36c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5f7f36c35c60bd75d11e60bcec11b0494e917f25))
* **package:** 更新libs包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2c42413](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2c424139741cc8f559bdb74937a78b5c8477c20f))
* **pinyin:** 修复pinyin-pro依赖引用问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([ef71e00](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ef71e00761753aeba11233596c8dda6967d7d468))
* **plan:** 修复产品方案切换后，刷新页面[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([c73b85c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c73b85ca0edd26d1cbc244880fdb5add30720c53))
* **plugin:** 增加plugin插件绑定JE[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0caaf66](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0caaf6687fd90a50f7829f8372e23695d837e99d))
* **proxy:** 只有在主应用才会启用子应用的调试代理地址 ([8def26b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8def26be5a7add2cc7a3996796bc0be75ac140ee))
* **remove:** 删除废弃文件[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([ffd4308](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ffd4308e0cbc9f63a15c83e9231067edfef19c95))
* **route:** 修复子应用菜单获取路由数据问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([4a61aff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4a61aff54ea364a48a79b4e3831e65071bc1415a))
* **router:** 调整路由，路由守卫统一交由common管理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2bcc274](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2bcc27495ebc6f571a1827e89d29caa87ecd9c43))
* **router:** 路由守卫自行处理[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([3900dcc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3900dcc52b313a1a80a86550d0f792b9e2b1e10b))
* **router:** 退出登录，不记录路由[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([677fcc5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/677fcc5d3de9e756845b81b6fff93b547f282900))
* **router:** 修复路由白名单引用错误[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([1d103d3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1d103d354d22e373c38f38ab6335074d1b9d2e99))
* **router:** 支持系统路由自定义配置，支持路由菜单 ([bcb37f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bcb37f77b2076b9d23ccfaf1f50cacaa5c373af9))
* **setting:** 调整系统设置的目录[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([e1128b2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e1128b23b7c29ca1f00189b78790cf2b907cfc68))
* **settings:** 调整系统设置按钮 ([41a8e2c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/41a8e2c09c2562ba580ae62a177b25ace854bad6))
* **sider:** 修复左侧菜单样式[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([2168439](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2168439d0d749939d93a614cdd52e44f786227f7))
* **static:** 修复由于主题插件导致打包失败问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([4f80a7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4f80a7ecb794eeac3efde7ea4bbb8179b184ba76))
* **style:** 调整系统滚动条的样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2d849a5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2d849a583b44b82ba8ceea8b16a714b716c38448))
* **style:** 调整scroll，button样式 ([ba0aaca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ba0aaca540345aade047b83300d629598ba93dfe))
* **style:** 开发模式下微应用不加载样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([19e0bd8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/19e0bd88ac37002f2e8e3631cd10f7b255a91f84))
* **style:** 修改style文件引用路径[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e3df4bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e3df4bc77cdbd6444cc3f6d6b77cafbf0117ce6a))
* **style:** 优化ui库的资源位置 ([c2890e3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c2890e35fe680260c5623f41cbc95e7bf39b83d4))
* **style:** 增加ant tabs样式调整 ([bf61f69](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bf61f694e5fdecbf033d239d6fd31e5aa24ba542))
* **style:** 组件包的样式引用修复 ([ef5fc45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ef5fc455408a91e5a42b0e4377b2cb2606baee1c))
* **system:** 系统的方法改用@jecloud/utils里的initSystem[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([b00e589](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b00e58957e732a252f23e550e5b01139f79c0bd8))
* **system:** 修改系统变量请求接口 ([b80e4e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b80e4e0bfa3dfa9a2988d51aa34ad1a88385aebf))
* **tabs:** 功能标签页去掉关闭按钮[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([d1588d8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d1588d87f0ffb244bbe15f5f7c56ac78d9cf9b9f))
* **tabs:** 修复tabs导航右键菜单问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([a2002f8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a2002f8cea6e6284ebd640195ed0abd67cc5f2fc))
* **theme:** 表格主题色调整[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2b80f27](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2b80f27e71f4b8082a1af54450b2905a14cee71d))
* **theme:** 调整表格主题色[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([ba82c8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ba82c8b22e7a46a851c39c939e340e2e892e9c6f))
* **theme:** 调整主题打包文件[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([b046aef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b046aefd0edd171fded0db61f33d7613d7eab051))
* **theme:** 将主题变量改为文件[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([2c587f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2c587f041935cff2ef7ed4b6b1cdb76284f5922a))
* **theme:** 确定主题支持方式[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([cb8d445](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/cb8d44556dde96e1ed7de7aa33f48f8ad4d9c028))
* **theme:** 修复代码构建主题文件目录错误 ([adf0bc4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/adf0bc472a68697dc338cc7beec2afbb19c18d9d))
* **theme:** 修复登录页的样式 ([5d416c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5d416c415d615fa1e66445d872a774d96c22435f))
* **theme:** 修复页面头部主题颜色 ([d9376c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d9376c060ce3cd383f53f39215e6238019be08a8))
* **theme:** 修复主题参数引用[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/2)] ([fa75c81](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fa75c816f81a9de52ea4b0211682ed8a844fb8a8))
* **theme:** 修复主题色样式 ([8383043](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/83830436cfb35570bf3a88667e269d86689db101))
* **theme:** 修复主题样式 ([a6fdef7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a6fdef7afcf7becc1b109ea669db9e87c0d16d15))
* **theme:** 修复子应用不设置主题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([90e4475](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/90e44752431eef8a828e509c540686f7ad0f2f06))
* **theme:** 修复左侧菜单背景样式问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([7cd2c52](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7cd2c52039edfaeccb05bdc83c7336a899d8d0e2))
* **theme:** 修改系统主题默认字体色为[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/3)f3f3f ([c5f0e6d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c5f0e6d17fc12ae7ca4ae14ecbd8cbfcfaad7306)), closes [#3f3f3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/3f3f3)
* **theme:** 修改主题文件引入错误 ([e421b35](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e421b35887cfdc8e270ba485741b60604905decd))
* **theme:** 修改ant主题变量引用 ([c75eaa4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c75eaa4a87bf86f7e0362b2c2b5e28fc1a43b705))
* **theme:** 优化主题色选择组件 ([a645eff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a645eff862ac9a71a0ec2084a62e6ceb7620c2de))
* **ui:** 绑定项目publishPath ([13f7d13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/13f7d13cf32fdc156d5ac67e98039eea0e1aa1d1))
* **ui:** 调整功能页面内边距为14px[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([20ed5a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/20ed5a6d749321343cb728aed2adc34b9b3caa7f))
* **uitls:** 修复timer使用方式[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([c0cdc9e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c0cdc9e8b7c311f70fa72ddd918cafb7b8f66e7b))
* **update:**  上传头像内容[[#18](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/18)] ([167c92f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/167c92f88b7bc9c830a18bd217ac0a86688fc53c))
* **update:** 单条消息提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([421fc16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/421fc1699bac75b9612ac17208c890f52f9ec3e4))
* **update:** 个人设置切换部门逻辑[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/10)] ([dcfae46](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dcfae46245a674a0c4a4818369867e13a6f63639))
* **update:** 个人设置头像UI[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([98256af](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/98256afc6ae35e858d3a7e7319d560036aa2926b))
* **update:** 个人设置样式修复[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([c0741ca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c0741ca13b87919420ebeb7398929af96b416237))
* **update:** 获取字段修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f77744e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f77744e4be6dd2cfa9c8e661121de57a8961dcc9))
* **update:** 解决流程通知换行问题[[#12](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/12)] ([3eb4de9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3eb4de9ea241f11395f943ba086f96a5602037ee))
* **update:** 解决流程通知换行问题[[#19](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/19)] ([554b0d7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/554b0d7b77da02ba82d866aec1b528d0c8d6c3f4))
* **update:** 解决语法报错问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([71b81ab](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/71b81ab5e815de2475676565dac7635b64f407a8))
* **update:** 解决语法问题[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/11)] ([89b65ad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/89b65ad40ec9664d159d38027ec20846a332d680))
* **update:** 去掉没用代码[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([db9988b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/db9988b5699a3533c0b25504dc4a624dafb61683))
* **update:** 去掉debugger[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([679247e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/679247e5e30730779f80a0c4f75400497e17c363))
* **update:** 删除调试代码[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([89147e6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/89147e6437b622f0e0901ac618804c1b12baf754))
* **update:** 删除没用代码[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0f27913](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0f27913797b9ae0d5396c663b237121bedf534d5))
* **update:** 头部背景图修改[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([81d1eef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/81d1eef6afd555f13d98a1b6e62a851cd27ef870))
* **update:** 头像传参处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([b958ca1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b958ca1ca1a3a0d5235d2eb37686aee851157b30))
* **update:** 文案修正[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([89b1002](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/89b10020266fe7e1e09bfe3580b775bae1c834df))
* **update:** 消息处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([1b99838](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1b9983845fdbde8bcf33516d65f2ff06af6a72f0))
* **update:** 消息代码注释[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e5c5b55](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e5c5b55660b8a020b52576cf6b4f26e280221268))
* **update:** 消息样式修复[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([37dfdf9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/37dfdf9553751a5f131764751a5388bc637a73e1))
* **update:** 修复个人设置的样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([b390f88](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b390f881a04d86c2935cc2f7313d4a27be1940a2))
* **update:** 修改触发应用的编码[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([6f3bdab](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6f3bdab00fa69bfa5b1fe188427cb15077f7147d))
* **update:** 修改右上角消息推送编码[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/9)] ([31a9fe1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/31a9fe1a4e0eb0b10622bbc3f33406d6c3fe8cef))
* **update:** 隐藏批注微邮[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([dd9e3ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dd9e3ff6f938972dde7e5155502118451b52ba05))
* **update:** 增加消息推送[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([b5295d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b5295d267f3fb96ff49d962e41e8ef201adeb6a2))
* **update:** 增加修改密码3S倒计时跳转的需求[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/11)] ([fc33021](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fc33021a6b641f0e8bdcc9cd915a5bda284b1806))
* **update:** 增加修改头像[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([9260bad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9260baddd5f8fe565877716d720146e010b875c3))
* **update:** 重新修改个人设置[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([7200e9e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7200e9ec6d3d9d48db5e982894dae6992d37226f))
* **websocket:** 删除通知消息重复属性[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([8c6726d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8c6726d6537d64d2e36b38f7021f15c73c28fb6c))
* **websocket:** 修复websocket监听方法[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0183ee6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0183ee689d59f085c209e764512f7f0e94638372))
* **websocket:** 修改消息体的参数说明[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([e82c19c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e82c19c25e653b208cab362c95ee5ee0ae43eb7b))
* **welcome:** 修复欢迎页的背景色和字体色[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([2ab3122](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2ab3122ee23db703b8d40ed28eeab8da81980c91))
* **workflow:** 移除@jecloud/workflow,增加@jecloud/plugin[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e69cbe2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e69cbe20caa548085bcf74d28a83a911d4596673))


### Features

* **admin:** 增加主应用打开菜单事件[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([7b1fab6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7b1fab641b8a6f61b845778eca408b262e153778))
* **admin:** 主子应用打通 ([79efb20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/79efb20c62636a1cb61af825fa8c4a1c77eeab0a))
* **ajax:** 统一前后端数据格式 ([937fd40](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/937fd40adfcb81a15aecbf02087579fa56413a6f))
* **ant:** 更新antd版本3.0.0-beta.9 ([92a29e8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/92a29e8b69eb7a5c9797aa377fca8c3feba0fd85))
* **antd:** ant-design-vue升级到3.1.0正式版 ([5cd3f38](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5cd3f380bd898364663e2905d8a85e86c630d689))
* **assets:** 增加微应用共享资源文件[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([86f890c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/86f890cecc2ccea8ef0e0ce37df8420152b1a490))
* **build:** 增加@jecloud/workflow样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([9b66b09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9b66b0909306d745bce60b9ab5170b7896d1eea5))
* **build:** 增加打包去除注释插件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([66282ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/66282ff1364b2d653d28f71e612f11c341ec612b))
* **build:** 增加微应用相关配置 ([137bc7f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/137bc7f5b5173a66a1b43a81301cb904a6fcd05d))
* **build:** 增加文件配置和路由守卫自定义入口 ([751b820](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/751b8203845909638919e2852ecfbf6e42eeb0a3))
* **demo:** 调整入口文件，增加demo页面 ([0d13adb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0d13adbb6a399296dca58d19550783214b0773c4))
* **doc:** 增加开发技巧等说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([06b95f5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/06b95f5c5dc6cd764587725e2d2f5265e7e5e420))
* **doc:** 增加开发技巧等说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f412d5a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f412d5ac1bddeabf7859265f4facd41785e122b3))
* **doc:** 增加git操作技巧[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0eaf65d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0eaf65d1c1cda085057cc566b73f3a7accbb60ce))
* **doc:** 增加websocket使用说明[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([6f0d2ca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6f0d2ca85dc4cd573ca59c7ef436c8cd13156fbc))
* **env:** 修改项目环境变量 ([2977806](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2977806880b15e4cb3febcff9143925210c5d5b6))
* **fonts:** 增加字体图标[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([6e87e81](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6e87e81c1d19dd1905e5f522a75a515809912801))
* **func:** 增加功能配置注册[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([ff5fa57](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ff5fa5700a0d5637bee33f78f81ce6f1d521fc06))
* **func:** 增加jecloud/func功能包 ([2a24cb2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2a24cb2ee1a9bae835b06043edf3fdbdd5f5e3da))
* **func:** useSystem增加功能操作函数[[#45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/45)] ([62502c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/62502c12bcd9e077b57e5bc2b6e400978bac170d))
* **i18n:** 增加国际化支持[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/3)] ([cfcc144](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/cfcc1448f85c829dbcb46d01b9f575362d3ea376))
* **icon:** 增加icons路由，去掉icons.html[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([7accc7d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7accc7d1fea492d0cbbd86b47c609388409abe69))
* **icons:** 增加图标使用帮助[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([e7b5895](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e7b589560f172b718cd7be07b34f061d8b84fcdf))
* **icons:** 增加jeicons图标 ([37c1055](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/37c105591bb681c37725774048244ddfd085657b))
* **je:** 功能组件绑定JE对象，用于事件操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([4efc10c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4efc10cde1099b2e88d49717d960c628295ee376))
* **je:** 混入JE系统方法useSystem[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([eb627a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/eb627a2e773b4a7d220260701a170e8703c9903c))
* **je:** 增加全局调试器方法useDebugger[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([11f63a0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/11f63a0674236b8dee7dce42555b011714bbf313))
* **je:** 增加emitMicroEvent方法[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([fbf1553](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fbf15534ae196efd8be60fa763294289d334f671))
* **je:** 增加watchWebSocket方法，只有主应用可用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([20e0a0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/20e0a0e1a897fb2b1eb488eeb8cd51e794982033))
* **log:** 增加菜单日志埋点[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([41f0473](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/41f047355228e374cdd85af8d5fa82900b1f604c))
* **login:** 优化登录操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([45b29d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/45b29d24bba9930bee9a1cf6b9c5b41e83a6519d))
* **login:** login增加部门选择[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([770df7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/770df7ea8a47ce2881b06ed2c0bb2ff9fbfa074c))
* **menu:** 菜单导航增加拖动排序[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([d58c354](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/d58c3543828374f01e39f42ac8f0cf290717594a))
* **menu:** 菜单导航整体逻辑完成 ([b296c73](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b296c730b78b623e1aa18534ca113aa2404f368e))
* **menu:** 菜单支持方案过滤[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([6029c48](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6029c484b9d2aaadcea598c46fde2ac9304894bf))
* **menu:** 菜单支持链接和iframe[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([7ac156c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/7ac156c52c404eed46dd53714ef954b515004d03))
* **menu:** 顶部菜单支持动态配置[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([0f880ec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0f880ecef73f527539c2e98ebf93b276895e2003))
* **menu:** 增加菜单功能控制[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([b450695](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b4506952ee4f46349740b3c8b98e787e0fe06242))
* **menu:** 增加菜单权限控制[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([2d04f08](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2d04f08f27c1fa4352ff30cbb3ac77104557b445))
* **menu:** 增加左侧菜单模块顺序的用户态数据[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([093c468](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/093c4681b1a1cd60b5264506256d0504309b08b5))
* **menu:** 支持字典功能菜单[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([4e87ace](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4e87acef2715415ee61f8f855f73c584dbd442c6))
* **menu:** 重构菜单框架[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([43f9cd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/43f9cd36f3462ca6849b1f5fa1e7758cf0daddd4))
* **micro:** 暴露micro-app的钩子函数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([59e609d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/59e609dbc541d7e15f3e6a56dd347309c67a5cc4))
* **micro:** 调整微应用框架qiankun改为micro-app[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([23630f2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/23630f21e7c79f380806caf9ac8424e9f7f9b9c3))
* **micro:** 微应用框架调整qiankun改为micro-app[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([2cc49f3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2cc49f31bb09656d30e74bd1b243f6bae8b88557))
* **micro:** 微应用支持触发其他微应用事件[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([184f3b9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/184f3b957326243289c6027e3c0490d63611c8ff))
* **micro:** 微应用支持触发其他微应用事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([0c54bf7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0c54bf77ae1733b1cccef5d2b45419bd31269b3b))
* **micro:** 微应用支持动态配置[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([b8f5c8d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b8f5c8d4986b275d20a0a1b76e7297d9b4df095d))
* **micro:** 增加首页展板配置[[#45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/45)] ([f19fd16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f19fd16c7590a46bc351f986947ba7f5a5fbb887))
* **micro:** 增加数据权限的配置信息[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([bf497ef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bf497efd166262483c26e6823b54c878a49e8f6c))
* **micro:** 增加主子应用通讯[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([2d7b3d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/2d7b3d2fd72563184e8adadcc78abe90175521b2))
* **micro:** 增加micro-app京东微应用框架源码[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([8fa8a6e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/8fa8a6e8562e67f0bfe1d019fd06f6b00d3c93cc))
* **micro:** 主子应用框架搭建完成 ([465ed40](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/465ed4034743e09d5a47bb68aea54f2bdcb7dfb9))
* **monaco:** 增加monaco静态资源，不再单独打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([c0961b0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/c0961b04dd161afc33a9e48a4baaf5e111888bee))
* **monaco:** 增加webpack，vite的monaco插件 ([1df85bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1df85bcf34dd7a68888de72026762dc6d05cf99d))
* **monaco:** 重写vite插件，修改路径引用错误问题 ([dea79f1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dea79f1790d42e4e896636112319ae9f4627b1e9))
* **mork:** 增加mork数据支持 ([3eb1d44](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3eb1d449abfea042263f3d42f091c001d23138c7))
* **mxgraph:** 增加mxgraph静态资源文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([e4cdc85](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e4cdc853b6db5d8f34ab2bd3c34a5ef1f07f766a))
* **nav:** 增加tabs导航条[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([dd32e6c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/dd32e6cfe7031f523ab8cad1568a72c3e3635cd6))
* **plan:** 增加系统方案动态路由[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([6981427](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/69814273ae134e93f72b7e90ea76e0d56b51f86a))
* **plan:** 增加globalStore的方案配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([a335bb5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a335bb5c9eeadcec12f55e17a97a1bb8ce0422b4))
* **plan:** 支持方案配置，用户态配置[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([521d2e7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/521d2e7b2754cf90dcdc3edaa535a743bb4013a4))
* **preview:** 增加本地预览服务 ([0cd7aa1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/0cd7aa141fc756e900323fb142fe62d69271844f))
* **preview:** 增加本地ip输出，方便调试 ([4afdc69](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/4afdc69fec00fbb0ff1daf85a368be9cec75c83d))
* **rbac:** 更新登录和获取用户接口[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([83a9597](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/83a9597c021c1c91e21f2e77a4614a675aaa0186))
* **route:** 菜单于路由绑定完成[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([3d58901](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/3d58901607e1811e71cb9fdac863d7b8182b1077))
* **route:** 增加菜单路由处理，未完 ([af28df5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/af28df5d01617f5577cc6d4ad98a17b1f72fd4c0))
* **router:** 支持用户自定义history ([1421921](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/142192104955f238e8112d959c1d55a35f1318fd))
* **router:** 支持自定义路由白名单[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([1ee13a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/1ee13a47dd250e2ea4ca2a3367d8bb455388a078))
* **setting:** 增加系统设置支持[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([21faa2c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/21faa2c25741ea8cdfaac1df5a6a4c73dc33def1))
* **sourcecode:** 增加微应用源代码，后期调试使用[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([262cef0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/262cef049454bbeeac11ca330faa7e6f2fbf94c8))
* **static:** 静态资源文件提取[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([f3e0edb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/f3e0edbbe6a20f33922449e56db057e37b777396))
* **static:** 增加静态资源包文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([28e62e5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/28e62e5b23fa933a368473623c2d152fad5e5d7a))
* **store:** 优化主子应用通讯问题[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([82eff8f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/82eff8f3b90397597d6d0150567c0cdbc70f3ac0))
* **theme:** 调整主题样式 ([380372a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/380372ace1dfdeec36be5c67ff02e3467bc77e9e))
* **theme:** 增加灰色模式，弱色模式[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([fa4d1df](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/fa4d1df85b69176cb5956e52ac77594b5f8307d2))
* **theme:** 增加系统主题设置 ([229ecdc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/229ecdce1f7183776b4b24bb4a873f2c5f9765b3))
* **theme:** 增加主题支持[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([bbfa033](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/bbfa0333e212e51b73355b4bb948f41933c5064b))
* **theme:** 增加vben主题设置[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/6)] ([ad34b2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ad34b2ebcb496b8e422421bd940af99ab4666798))
* **theme:** 主题组件增加切换主题事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([ef9ac87](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/ef9ac87c6ae84c2f14de177d0ed89312ca7a0f30))
* **theme:** vuecli支持主题配置[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/7)] ([b361a5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/b361a5f0e2e976884a25e4494d2fa003b8a583ce))
* **tinymce:** 增加tinymce插件，更新antd版本 ([13f390a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/13f390a11f4d131177ab94cde91ef694ace3a2a5))
* **version:** release v1.0.0 ([67286f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/67286f00e4b82757db2bfde72abea413874cb6b9))
* **version:** release v1.0.0 ([a969009](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/a96900905fea3ce679206313ddd57c2ec0504011))
* **websocket:** 增加通知消息提示音[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([e9df82f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/e9df82f04d3a74a1c116646a6c23506be3c66345))
* **websocket:** 增加websocket代理支持[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([9dd73ed](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/9dd73edccd88e8ec37c13f70ce9068525e04d2bb))
* **websocket:** 增加websocket支持[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/4)] ([5bf175a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/5bf175a363b5a5e29f89dbe2780cb9ffa9d3d9b3))
* **workflow:** 增加@jecloud/worfkow[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/issues/8)] ([6d1aea5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-admin/commit/6d1aea5c6719b6115ffe84a89c6f9b0c28861267))




const InjectPlugin = require('webpack-inject-plugin').default;
// 动态引入版权校验
const plugin = new InjectPlugin(function () {
  return "import('./build/license/jecloud.license.js');";
});

module.exports = {
  build(config) {
    if (process.env.NODE_ENV === 'production') {
      config.configureWebpack.plugins.push(plugin);
    }
  },
};

import { useJE, timer, JeWebSocket, decode } from '@jecloud/utils';
import { onMounted, nextTick } from 'vue';
// 重写接收方法
const onReceive = JeWebSocket.prototype.onReceive;
JeWebSocket.prototype.onReceive = function (message) {
  if (message.type === 2 && message.busType === 'jecloud-license') {
    setLicense(message.content);
  } else {
    onReceive.call(this, message);
  }
};

timer(() => {
  const app = useJE().$vue;
  if (app) {
    app.mixin({
      beforeCreate() {
        const { name } = this.$options;
        const component = this;
        if (name === 'AdminSettings') {
          onMounted(() => {
            nextTick(() => {
              loadLicense().then((info) => {
                createSupportDom({
                  parent: component.$el,
                  info,
                });
              });
            });
          });
        }
      },
    });
    return false;
  }
}, 100);
let licenseInfo;
/**
 * 安装证书
 * @param {*} info
 */
function setLicense(info) {
  licenseInfo = decode(info);
  loadLicense();
}
/**
 * 加载证书信息
 * @returns
 */
function loadLicense() {
  if (!licenseInfo?.LICENSE) {
    const message = '加载jecloud.license证书失败！';
    alert(message);
    return Promise.reject(message);
  } else {
    return Promise.resolve(licenseInfo);
  }
}

/**
 * 创建技术支持DOM
 * @param {*} parent
 */
function createSupportDom({ parent, info }) {
  const { COMPANY, COMPANY_URL } = info;
  // 技术支持dom
  const div = document.createElement('div');
  div.setAttribute('style', 'padding:20px 0 10px 0;color:#a9aba9;text-align:center;');
  if (COMPANY_URL) {
    div.style.cursor = 'pointer';
    div.onclick = function () {
      window.open(COMPANY_URL);
    };
  }
  div.innerHTML = COMPANY;
  // 插入技术支持
  parent.insertBefore(div, parent.lastChild);
  // dom监听
  const domChangeEvent = () => {
    // 停止监听
    mo.disconnect();
    // 重新注入
    createSupportDom({ parent, info });
  };
  // 监听dom删除,DOMNodeRemoved
  div.addEventListener('DOMNodeRemoved', domChangeEvent);
  // 监听属性变化,MutationObserver
  const mo = new window.MutationObserver(domChangeEvent);
  mo.observe(div, {
    characterData: true, // 子节点的字符
    childList: true, // 观察目标子节点的变化，添加或者删除
    attributes: true, // 观察属性变动
    subtree: true, // 默认为 false，设置为 true 可以观察后代节点
  });
}

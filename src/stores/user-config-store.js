import { defineStore } from 'pinia';
import { editUserConfig } from '../data/api/setting';
/**
 * 用户态配置
 */
export const useUserConfigStore = defineStore({
  id: 'user-config-store',
  state: () => ({
    systemTheme: '', // 系统主题
    siderTheme: '', // 菜单主题
    siderMenuSort: '', // 左侧菜单顺序
  }),
  actions: {
    setSystemTheme(configInfo) {
      this.systemTheme = configInfo;
      editUserConfig({ type: 'systemTheme', configInfo });
    },
    setSiderTheme(configInfo) {
      this.siderTheme = configInfo;
      editUserConfig({ type: 'siderTheme', configInfo });
    },
    setSiderMenuSort(configInfo) {
      this.siderTheme = configInfo;
      editUserConfig({ type: 'siderMenuSort', configInfo });
    },
    init(data = []) {
      data.forEach?.((item) => {
        this[item.type] = item.configInfo;
      });
    },
  },
});

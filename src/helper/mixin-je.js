import { ref } from 'vue';
import { useAdminStore } from '@/stores/admin-store';
import { useWebSocketStore } from '@/stores/websocket-store';
import { mixinJE, useJE } from '@common/helper/je';
import { loadDynamicApp, openMicroApp } from '@/hooks/use-micro';
import { openAdminMenu } from '@/hooks/use-menu';
import { getAjaxInstance } from '@jecloud/utils';
import { logout } from './system';
import { useMenuStore } from '@/stores/menu-store';
import { useRouter } from '@common/router';
/**
 * 主应用特有方法
 * @returns
 */
function useAdmin() {
  const adminStore = useAdminStore();
  const menuStore = useMenuStore();
  return {
    adminStore,
    menuStore,
    $router: useRouter(),
    useJE,
    /**
     * 触发微应用事件
     * @param {*} name
     * @param  {...any} args
     */
    emitMicroEvent(name, ...args) {
      return adminStore.emitMicro(name, ...args);
    },
    /**
     * 是否包含微应用配置
     * @param {*} name
     * @param  {...any} args
     */
    hasMicroConfig(name, ...args) {
      return adminStore.hasMicroConfig(name, ...args);
    },
    /**
     * 打开微应用
     */
    openMicroApp,
    /**
     * 动态加载app
     */
    loadDynamicApp,
    /**
     * 打开菜单
     */
    openMenu: openAdminMenu,
    watchWebSocket,
    useDebugger,
  };
}

/**
 * 触发微应用事件
 * @param {*} name
 * @param  {...any} args
 */
function emitMicroEvent(name, ...args) {
  const adminStore = useAdminStore();
  adminStore.emitMicro(name, ...args);
}
/**
 * 监听websocket消息
 * @param {*} watchFn
 * @returns
 */
const watchWebSocket = (watchFn) => {
  const webSocketStore = useWebSocketStore();
  return webSocketStore.addWatch(watchFn);
};

/**
 * 混入系统方法到JE
 */
export function mixinJE4Admin() {
  const system = useJE().useSystem();

  mixinJE({
    // TODO: 后期会废弃
    useSystem() {
      return { emitMicroEvent, watchWebSocket, ...system };
    },
    useAdmin,
  });

  // 注册主应用ajax注销操作
  const ajaxInstance = getAjaxInstance();
  // 自定义ajax catch函数
  ajaxInstance.offResponseCatch();
  ajaxInstance.onResponseCatch(({ error, config }) => {
    console.error('AJAX-ERROR', { ...error, config });
    // 用户失效，退出登录
    if (error.status == 401) {
      logout();
    }
  });
}

/**
 * 启用调试模式
 * @returns
 */
const debug = ref(false);
export function useDebugger() {
  return debug;
}

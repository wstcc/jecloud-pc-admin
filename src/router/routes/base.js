import Layout from '@/views/layouts/layout.vue';
import Page from '@/views/pages/page.vue';
import Page404 from '@/views/pages/system/404.vue';
import Page403 from '@/views/pages/system/403.vue';
import PageLogin from '@/views/pages/login.vue';
import PageThirdLogin from '@/views/pages/third-login.vue';
import PageBlank from '@/views/pages/blank.vue';
import baseRoutes from '@common/router/routes';
import Markdown from '@/views/pages/preview/markdown.vue';
import File from '@/views/pages/preview/file.vue';
import { GlobalSettingsEnum } from '@common/helper/constant';
const { GLOBAL_SETTINGS_TOKENKEY: tokenKey } = GlobalSettingsEnum;

/**
 *  系统路由说明
 * /main/模块/功能   功能路由
 * /main/home/功能   首页功能路由
 * /login 登录路由
 * /whitelist/preview/markdown/数据id 预览markdown文档
 */

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/je/main',
  },
  {
    path: '/login',
    name: 'Login',
    redirect: '/je/login',
  },
  {
    path: '/:plan',
    redirect(to) {
      return { name: 'PlanHome', params: { plan: to.params.plan, layout: 'main' } };
    },
  },
  {
    path: '/whitelist/preview/markdown/:id?/:planCode?',
    name: 'PreviewMarkdown',
    component: Markdown,
  },
  {
    // token认证路由
    path: `/${tokenKey}/:token/:redirect*`,
    name: tokenKey,
    component: PageBlank,
  },
  {
    path: '/:plan/:layout',
    component: Layout,
    name: 'PlanHome',
    children: [
      {
        path: ':menu/:view?/:id?',
        component: Page,
      },
      {
        path: '403',
        name: 'Plan403',
        component: Page403,
      },
      {
        path: '404',
        name: 'Plan404',
        component: Page404,
      },
    ],
  },
  {
    path: '/:plan/login',
    name: 'PlanLogin',
    component: PageLogin,
  },
  {
    path: '/whitelist/:plan/third/:tempCode',
    name: 'ThirdLogin',
    component: PageThirdLogin,
  },
  {
    path: '/whitelist/preview/file/:keys/:key',
    name: 'PreviewFile',
    component: File,
  },
  {
    path: '/404',
    name: '404',
    component: Page404,
  },
  // 将匹配所有内容并将其放在 `$route.params.pathMatch` 下
  {
    path: '/:pathMatch(.*)*',
    redirect(to) {
      const { plan, layout } = to.params;
      if (plan) {
        return { name: 'Plan404', params: { plan, layout } };
      } else {
        return { name: '404' };
      }
    },
  },
  // 公共路由
  ...baseRoutes.filter((route) => route.name !== 'Login'),
];

export default routes;

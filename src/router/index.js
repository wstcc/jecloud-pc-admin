import { createWebHashHistory } from 'vue-router';
import { setupRouter as _setupRouter } from '@common/router';
import routes from './routes';
import { createRouterGuard } from './guard';

/**
 * 注册路由
 *
 * @export
 * @return {*}
 */
export function setupRouter(app) {
  _setupRouter(app, {
    history: createWebHashHistory(),
    routes,
    guards: createRouterGuard,
  });
}

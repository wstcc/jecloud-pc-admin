import { nextTick, onMounted, onUnmounted, ref } from 'vue';
import Sortable from 'sortablejs';
export function useTabs() {
  const el = ref();
  let sortable;
  const initSortable = () => {
    const dragDom = el.value.$el.querySelector('.ant-tabs-nav-list');
    sortable = Sortable.create(dragDom, {
      handle: '.ant-tabs-tab',
      ghostClass: 'ant-tabs-tab-sortable-ghost',
    });
  };
  const destroySortable = () => {
    sortable?.destroy();
  };

  onMounted(() => {
    nextTick(() => {
      initSortable();
    });
  });

  onUnmounted(() => {
    destroySortable();
  });

  return { el };
}

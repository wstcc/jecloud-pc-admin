import { ref } from 'vue';
import { defineComponent } from 'vue';
import { loadSecurityMicro } from '@jecloud/utils';
/**
 * 用户密级标记
 */
export default defineComponent({
  setup() {
    let rendered = ref(false);
    let security = null;
    loadSecurityMicro().then((plugin) => {
      security = plugin;
      rendered.value = !!plugin;
    });

    return () => (rendered.value ? security.renderUserMark() : null);
  },
});

//手机号校验
export const LOGIN_VALIDA_PHONE =
  /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/;

/**
 * 简单密码策略(数字+字母，至少N位) /^(?=.*[a-z])(?=.*\\d)[a-z\\d]{%s,}$/;
 */
export function passwordSimpleRegTempalte(l) {
  return RegExp('^(?=.*[a-z])(?=.*\\d)[a-z\\d]{' + l + ',}$');
}

/**
 * 中等密码策略（数字+大小写字母）/^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d]{%s,}$/
 */
export function passwordMedinmRegTempalte(l) {
  return RegExp('^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d]{' + l + ',}$');
}
/**
 * 复杂密码策略（数字+大小写字母+特殊字符）/^(?=.*[$@$!%*#?&])(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d$@$!%*#?&]{%s,}$/
 */
export function passwordSimpleComplexTempalte(l) {
  return RegExp(
    '^(?=.*[$@$!%*#?^&_+])(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d$@$!%*#?^&_+]{' + l + ',}$',
  );
}
/**
 * 验证字符串中是否包含数字
 */
export function containsNumber(str) {
  const reg = /^[\d]+$/;
  return reg.test(str);
}
/**
 * 验证字符串中是否包含大小写字母
 */
export function constainsLetter(str) {
  const reg = /^[A-Za-z]+$/;
  return reg.test(str);
}
/**
 * 验证是否包含特殊字符（英文）
 */
export function constainsChars(str) {
  const regEn = /^[@$!%*#?^&_+]+$/;
  return regEn.test(str);
}
/**
 * 验证只包含特殊字符串和数字
 */
export function constainsNumberAndChars(str) {
  // const regNC = /^(?!([^(@$!%*#?^&_+)])+$)(?![0-9]+$)/;
  // const regNC = /^(?=.*[0-9])(?=.*[`~!@#$%^&*_+])[[0-9]`~!@#$%^&*_+<>?] +$/;
  const regNC = /[@$!%*#?^&_+].*[0-9]|[0-9].*[@$!%*#?^&_+]/;
  return regNC.test(str);
}
/**
 * 验证只包含大写字母和数字
 */
export function constainsNumberAndLetter(str) {
  const regNL = /^([?!(0-9A-Z)])+$/;
  return regNL.test(str);
}
/**
 * 验证只包含特殊字符和字母
 */
export function constainsLetterAndChars(str) {
  const regLC = /^(?!([^(@$!%*#?^&_+)])+$)(?![a-zA-Z]+$)/;
  // const regNC = /^(?=.*[A-Za-z])(?=.*[`~!@#$%^&*_+?:"[\]])[A-Za-z`~!@#$%^&*_+?:"]$/;
  return regLC.test(str);
}
/**
 * 验证只包含特殊字符和小写字母+数字
 * /^(?=.*[A-Za-z])(?=.*\d)(?=.*[`~!@#$%^&*()_+<>?:"{},.\/\\;'[\]])[A-Za-z\d`~!@#$%^&*()_+<>?:"{},.\/\\;'[\]]{8,}$/
 */
export function constainsLowerLetterAndCharNum(str) {
  const regLower = /^(?![a-z]+$)(?![0-9]+$)(?!([^(@$!%*#?^&_+)])+$)/;
  return regLower.test(str);
}
/**
 * 验证只包含特殊字符和大写字母+数字
 */
export function constainsLargeLetterAndCharsNum(str) {
  const regLarg = /^(?![0-9]+$)(?![A-Z]+$)(?!([^(@$!%*#?^&_+)])+$)/;
  return regLarg.test(str);
}
//hex转化rgba
export function hexToRgba(hex, opacity) {
  return (
    'rgba(' +
    parseInt('0x' + hex.slice(1, 3)) +
    ',' +
    parseInt('0x' + hex.slice(3, 5)) +
    ',' +
    parseInt('0x' + hex.slice(5, 7)) +
    ',' +
    opacity +
    ')'
  );
}

/**
 * API_URL命名规则：API_模块_方法
 */
//获取登录方案配置
export const API_META_API_LOGIN_LOADPLANS = '/je/meta/settingPlan/loadPlanAndLoginConfig';
//修改密码
export const API_RBAC_EDIT_LOGON_PASSWORD = '/je/rbac/cloud/account/modifyPassword';
// 加载个人样式
export const API_RBAC_LOAD_PERSONAL_SETTING = '/je/rbac/personalSettings/loadStyles';
// 更改主题颜色
export const API_RBAC_CHANGE_PERSONAL_STYLE = '/je/rbac/personalSettings/changeStyles';
// 更改头像
export const API_RBAC_CHANGE_PHOTO = '/je/rbac/cloud/account/updateUserAvatar';
// 获取是否显示小红点
export const API_MESSAGE_LOAD_READ_SIGN = '/je/message/homePortal/loadReadSign';
//  首页右上角消息通知小红点--清除
export const API_MESSAGE_UPDATE_SIGN = '/je/message/homePortal/doUpdateSign';
// 获取部门获取列表
export const API_RBAC_GET_CHNAGE_DEPT_LIST = '/je/rbac/cloud/login/getAccountDeptTree';
// 切换部门
export const API_RBAC_SWITCH_DEPT = '/je/rbac/cloud/login/switchDept';

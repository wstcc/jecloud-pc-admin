/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  API_META_API_LOGIN_LOADPLANS,
  API_RBAC_EDIT_LOGON_PASSWORD,
  API_RBAC_LOAD_PERSONAL_SETTING,
  API_RBAC_CHANGE_PERSONAL_STYLE,
  API_RBAC_CHANGE_PHOTO,
  API_MESSAGE_LOAD_READ_SIGN,
  API_MESSAGE_UPDATE_SIGN,
  API_RBAC_GET_CHNAGE_DEPT_LIST,
  API_RBAC_SWITCH_DEPT,
} from './urls';

/**
 * 切换部门列表数据获取
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getChangeDeptList(params) {
  return ajax({ url: API_RBAC_GET_CHNAGE_DEPT_LIST, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 切换部门获取
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function editSwitchDept(params) {
  return ajax({ url: API_RBAC_SWITCH_DEPT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 首页右上角消息通知小红点加载
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadReadSign(params) {
  return ajax({ url: API_MESSAGE_LOAD_READ_SIGN, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 首页右上角消息通知小红点--清除
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdateSign(params) {
  return ajax({ url: API_MESSAGE_UPDATE_SIGN, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 修改头像
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function changeUserPhoto(params) {
  return ajax({ url: API_RBAC_CHANGE_PHOTO, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取登录方案配置
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function GetLoginPlans(params) {
  return ajax({ url: API_META_API_LOGIN_LOADPLANS, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 修改密码
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function editPassword(params) {
  return ajax({ url: API_RBAC_EDIT_LOGON_PASSWORD, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 加载个人样式
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadPersonalSettings(params) {
  return ajax({ url: API_RBAC_LOAD_PERSONAL_SETTING, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 修改样式
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function editPersonalStyleSetting(params) {
  return ajax({ url: API_RBAC_CHANGE_PERSONAL_STYLE, params: params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

import { inject, provide } from 'vue';

export const SiderMenuContextKey = Symbol('jeSiderMenuContextKey');

export const useProvideSiderMenu = (state) => {
  provide(SiderMenuContextKey, state);
};

export const useInjectSiderMenu = () => {
  return inject(SiderMenuContextKey, null);
};

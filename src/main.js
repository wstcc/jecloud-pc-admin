import { createApp } from 'vue';
import { setupCommon } from '@common/helper';
import { setupRouter } from './router';
import { startMicroApp, initMicroConfig } from './hooks/use-micro';
import { mixinJE4Admin } from './helper/mixin-je';
import { setupWebSocket } from './helper/websocket';
import ui from '@jecloud/ui';
import func from '@jecloud/func';
import App from './app.vue';
// 初始化微应用
startMicroApp();
// Init Vue
const vue = createApp(App);
// 注册 Vue
vue.use(ui).use(func);
// Common
setupCommon(vue).then(() => {
  // websocket
  setupWebSocket();
  // Router
  setupRouter(vue);
  // 绑定系统功能
  mixinJE4Admin();
  // Mount
  initMicroConfig().then(() => {
    vue.mount('#app');
  });
});

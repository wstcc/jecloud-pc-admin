import { appInstanceMap } from '../create_app';
import { getRootContainer } from './utils';

function unmountNestedApp() {
  releaseUnmountOfNestedApp();

  appInstanceMap.forEach((app) => {
    // @ts-ignore
    app.container && getRootContainer(app.container).disconnectedCallback();
  });

  !window.__MICRO_APP_UMD_MODE__ && appInstanceMap.clear();
}

// if micro-app run in micro application, delete all next generation application when unmount event received
export function listenUmountOfNestedApp() {
  if (window.__MICRO_APP_ENVIRONMENT__) {
    window.addEventListener('unmount', unmountNestedApp, false);
  }
}

// release listener
export function releaseUnmountOfNestedApp() {
  if (window.__MICRO_APP_ENVIRONMENT__) {
    window.removeEventListener('unmount', unmountNestedApp, false);
  }
}

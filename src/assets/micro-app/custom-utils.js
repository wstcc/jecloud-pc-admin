import CreateApp, { appInstanceMap } from './create_app';
/**
 * 加载微应用
 * @param {*} param0
 * @returns
 */
export function loadMicroApp({
  name,
  url,
  container,
  props,
  inline = false,
  scopecss = false,
  useSandbox = true,
}) {
  // 获取服务地址
  if (!url.startsWith('http')) {
    url = window.location.origin + url;
  }
  /**
   * actions for destory old app
   * fix of unmounted umd app with disableSandbox
   */
  if (appInstanceMap.has(name)) {
    appInstanceMap.get(name)?.actionsForCompletelyDestroy();
  }

  const instance = new CreateApp({
    name,
    url,
    container,
    inline,
    scopecss,
    useSandbox,
    props,
  });

  appInstanceMap.set(name, instance);
  return instance;
}
export class Deferred {
  promise;

  resolve;

  reject;

  constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }
}

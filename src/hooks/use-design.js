export function useDesign(scope) {
  const values = { prefixCls: 'je' };
  return {
    prefixCls: `${values.prefixCls}-${scope}`,
    prefixVar: values.prefixCls,
  };
}

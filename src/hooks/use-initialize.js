import { useMenu } from './use-menu';
import { initGlobalEvents } from './use-events';
import { usePlan } from './use-plan';
export function useInitialize() {
  // 主应用全局事件
  initGlobalEvents();
  // 菜单路由处理
  const { watchHistoryMenu, watchMenuRoute } = useMenu();
  // 菜单路由
  watchMenuRoute();
  // 菜单历史记录
  watchHistoryMenu();

  // 方案处理
  const { watchActivePlan } = usePlan();
  watchActivePlan();
}

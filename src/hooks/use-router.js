import { useRouter as _ } from 'vue-router';
import { BLANK, MAINLAYOUT } from '@/router/constant';
import { useMenuStore } from '@/stores/menu-store';
import { LayoutMenuType } from '@/data/enum/menu';
/**
 * 安装路由
 * @param {*} router
 */
export const setupRoutes = function (router) {
  const menuStore = useMenuStore();
  menuStore.topMenus.forEach((menu) => {
    if (menu.leaf) {
      router.addRoute('main', {
        path: transtomPath(menu),
        name: menu.id,
        component: BLANK,
        meta: {
          menu: menu,
          topMenu: menu,
        },
      });
    } else {
      const routes = [];
      const { siderRootMenu } = menu;
      siderRootMenu?.cascadeNodes(function (node) {
        if (node.leaf) {
          routes.push({
            path: transtomPath(node),
            name: node.id,
            component: BLANK,
            meta: { menu: node, topMenu: menu },
          });
        }
      });
      router.addRoute('main', {
        path: menu.id,
        name: menu.id,
        component: MAINLAYOUT,
        children: routes,
      });
    }
  });
};
/**
 * 格式化路由路径
 *
 * @param {*} menu
 * @return {*}
 */
const transtomPath = function (menu) {
  let path = '';
  if (menu.modelType === LayoutMenuType.TOP) {
    path = `home/${menu.id}`;
  } else {
    path = menu.id;
  }
  if (menu.micro) {
    path += '/:path(.*)*';
  }
  return path;
};

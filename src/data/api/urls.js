/**
 * API_URL命名规则：API_模块_方法
 */

/*--------------------------------------- 菜单 ---------------------------------------*/
/**
 * 左侧菜单
 */
export const API_RBAC_MENU = '/je/rbac/menu/loadHomeMenus';
/**
 * 顶部菜单
 */
export const API_RBAC_MENU_TOP = '/je/rbac/menu/loadHomeTopMenus';
/**
 * 收藏菜单
 */
export const API_RBAC_MENU_STAR = '/je/rbac/menu/quick/loadUserQuickMenus';
/**
 * 收藏/取消收藏
 */
export const API_RBAC_MENU_STAR_COLLECT = '/je/rbac/menu/quick/collectUserQuickMenu';
/**
 * 更新收藏菜单顺序
 */
export const API_RBAC_MENU_STAR_UPDATE = '/je/rbac/menu/quick/updateQuickMenus';
/**
 * 批量移除收藏菜单
 */
export const API_RBAC_MENU_STAR_REMOVE = '/je/rbac/menu/quick/removeQuickMenus';

/**
 * 历史菜单
 */
export const API_RBAC_MENU_HISTORY = '/je/rbac/menu/history/loadUserMenuHistory';
/**
 * 记录菜单历史
 */
export const API_RBAC_MENU_HISTORY_RECORD = '/je/rbac/menu/history/recordMenuHistory';
/**
 * 删除菜单历史
 */
export const API_RBAC_MENU_HISTORY_REMOVE = '/je/rbac/menu/history/removeMenuHistory';

/*--------------------------------------- 系统设置 ---------------------------------------*/
/**
 * 系统方案配置
 */
export const API_META_SETTING_PLAN_CONFIG = '/je/meta/settingPlan/loadPlanAndLoginConfig';

/*--------------------------------------- 用户设置 ---------------------------------------*/
/**
 * 加载用户配置
 */
export const API_META_USER_LOAD_CONFIG = '/je/meta/userInfo/loadUserConfigInfo';
/**
 * 编辑用户配置
 */
export const API_META_USER_EDIT_CONFIG = '/je/meta/userInfo/editUserConfigInfo';

/*--------------------------------------- 功能通用接口 ---------------------------------------*/

/**
 * 查询
 */
export const API_COMMON_LOAD = '/je/common/load';

/*--------------------------------------- 微应用接口 ---------------------------------------*/
/**
 * 加载微应用
 */
export const API_META_MICROAPP_LOAD_MICROAPP = '/je/meta/microApp/loadMicroApp';

/*--------------------------------------- 获得markdown数据 ---------------------------------------*/
/**
 * 获得markdown数据
 */
export const API_PREVIEW_MARKDOWN_GETAPIMARKDOWN = '/je/api/getApiMarkdown';

/*--------------------------------------- 文件预览 ---------------------------------------*/
/**
 * 更加文件key获取文件数据
 */
export const API_DOCUMENT_PREVIEW_FILEDATA = '/je/document/getFileInfo';

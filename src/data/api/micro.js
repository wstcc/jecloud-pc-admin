import { ajax } from '@jecloud/utils';
import { transformData } from '@common/helper/http';
import { API_META_MICROAPP_LOAD_MICROAPP } from './urls';
/**
 * 加载微应用
 * @returns
 */
export function loadMicroApp() {
  return ajax({ url: API_META_MICROAPP_LOAD_MICROAPP, method: 'GET', token: false }).then(
    transformData,
  );
}

/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import { transformData } from '@common/helper/http';
import { useGlobalStore } from '@common/store/global-store';
import {
  API_RBAC_MENU,
  API_RBAC_MENU_STAR,
  API_RBAC_MENU_STAR_COLLECT,
  API_RBAC_MENU_STAR_REMOVE,
  API_RBAC_MENU_STAR_UPDATE,
  API_RBAC_MENU_HISTORY,
  API_RBAC_MENU_HISTORY_RECORD,
  API_RBAC_MENU_HISTORY_REMOVE,
  API_RBAC_MENU_TOP,
} from './urls';
/**
 * 根据菜单获得方案信息
 * @param {*} menuId
 * @returns
 */
export function getPlanByMenuId(menuId) {
  return ajax({ url: '/je/rbac/menu/getMenuScheme', params: { id: menuId }, method: 'GET' }).then(
    transformData,
  );
}

/**
 * 菜单数据
 *
 * @export
 * @return {Promise}
 */
export function loadMenus() {
  return ajax({ url: API_RBAC_MENU, params: { plan: getActivePlan() } }).then(transformData);
}

/**
 * 顶部菜单数据
 *
 * @export
 * @return {Promise}
 */
export function loadTopMenus() {
  return ajax({
    url: API_RBAC_MENU_TOP,
    params: { plan: getActivePlan() },
  }).then(transformData);
}

/**
 * 收藏菜单
 *
 * @export
 * @return {Promise}
 */
export function loadStarMenus() {
  return ajax({ url: API_RBAC_MENU_STAR, params: { plan: getActivePlan() } }).then(transformData);
}
/**
 * 收藏,取消收藏菜单
 * @param {*} menuId
 * @returns
 */
export function collectStarMenu(menuId, collect) {
  return ajax({
    url: API_RBAC_MENU_STAR_COLLECT,
    params: { menuId, type: collect ? 'collect' : 'uncollect' },
  }).then(transformData);
}
/**
 * 删除收藏菜单
 * @param {*} menuId
 * @returns
 */
export function removeStarMenu(menuId) {
  return ajax({ url: API_RBAC_MENU_STAR_REMOVE, params: { menuIds: menuId } }).then(transformData);
}
/**
 * 批量更新收藏菜单
 * @param {*} data
 * @returns
 */
export function updateStarMenus(menuIds) {
  return ajax({ url: API_RBAC_MENU_STAR_UPDATE, params: { menuIds } }).then(transformData);
}

/**
 * 历史菜单
 *
 * @export
 * @return {Promise}
 */
export function loadHistoryMenus() {
  return ajax({ url: API_RBAC_MENU_HISTORY, params: { plan: getActivePlan() } }).then(
    transformData,
  );
}

/**
 * 删除历史菜单
 *
 * @export
 * @return {Promise}
 */
export function removeHistoryMenu(menuId) {
  return ajax({ url: API_RBAC_MENU_HISTORY_REMOVE, params: { menuIds: menuId } }).then(
    transformData,
  );
}

/**
 * 记录历史菜单
 *
 * @export
 * @return {Promise}
 */
export function recordHistoryMenu(menuId) {
  return ajax({ url: API_RBAC_MENU_HISTORY_RECORD, params: { menuId } }).then(transformData);
}
/**
 * 获取当前方案
 * @returns
 */
function getActivePlan() {
  return useGlobalStore().activePlan;
}

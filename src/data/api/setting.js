import { ajax } from '@jecloud/utils';
import { transformData } from '@common/helper/http';
import {
  API_META_SETTING_PLAN_CONFIG,
  API_META_USER_EDIT_CONFIG,
  API_META_USER_LOAD_CONFIG,
} from './urls';
/**
 * 根据方案加载登录配置
 * @param {*} plan 方案编码
 * @returns
 */
export function loadPlanConfig(plan = 'je') {
  return ajax({ url: API_META_SETTING_PLAN_CONFIG, params: { code: plan }, token: false }).then(
    transformData,
  );
}
/**
 * 加载用户态设置
 * @param {String} type 配置类型
 * @returns Array
 */
export function loadUserConfig(type) {
  return ajax({ url: API_META_USER_LOAD_CONFIG, params: { type } }).then(transformData);
}
/**
 * 编辑用户态配置
 * @param {String} type 配置类型
 * @param {String} configInfo 配置信息
 * @returns Object
 */
export function editUserConfig({ type, configInfo }) {
  return ajax({ url: API_META_USER_EDIT_CONFIG, params: { type, configInfo } }).then(transformData);
}

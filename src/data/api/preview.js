import { ajax } from '@jecloud/utils';
import { transformData } from '@common/helper/http';
import { API_PREVIEW_MARKDOWN_GETAPIMARKDOWN, API_DOCUMENT_PREVIEW_FILEDATA } from './urls';
/**
 * 获得markdown数据
 * @returns
 */
export function getMarkdownData(params) {
  return ajax({ url: API_PREVIEW_MARKDOWN_GETAPIMARKDOWN, params, method: 'GET' }).then(
    transformData,
  );
}

/**
 * 根据文件key获得对应文件数据
 * @returns
 */
export function getFileDataByKeys(params) {
  return ajax({ url: API_DOCUMENT_PREVIEW_FILEDATA, params }).then((info) => {
    return info;
  });
}

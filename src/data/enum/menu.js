/**
 * 布局菜单类型
 * @enum
 */
export const LayoutMenuType = Object.freeze({
  TOP: 'TOP',
  SIDER: 'SIDER',
});

/**
 * 功能菜单类型
 */
export const MenuType = Object.freeze({
  MENU: 'MENU', // 菜单
  FUNC: 'FUNC', // 功能
  FUNCFORM: 'FORM', // 功能表单
  PLUGIN: 'PLUGIN', // 插件
  URL: 'URL', // 链接
  IFRAME: 'IFRAME',
  PORTAL: 'PORTAL',
  DIC: 'DIC',
  CHART: 'CHART',
  REPORT: 'REPORT',
});

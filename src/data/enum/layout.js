/**
 * 应用布局
 */
export const LayoutType = Object.freeze({
  /**
   * 所有菜单布局
   */
  MAIN: 'main',
  /**
   * 顶部菜单布局
   */
  HOME: 'home',
  /**
   * 单应用
   */
  APP: 'app',
  /**
   * 单应用，无头
   */
  SINGLE: 'single',
});

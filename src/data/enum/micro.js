/**
 * 微应用激活方式
 */
export const MicroActiveType = Object.freeze({
  INIT: 'init',
  MENU: 'menu',
  LOGIN: 'login',
  PLUGIN: 'plugin',
});

import { h } from 'vue';
import { decode } from '@jecloud/utils';
/**
 * 通知消息
 */
export default class NoticeMessage {
  /**
   * 标题
   */
  message;
  /**
   * 内容
   */
  description;
  /**
   * 内容
   */
  content = '';
  /**
   * 允许关闭
   */
  closable = false;
  /**
   * 延迟关闭时间
   */
  duration = null;
  /**
   * 状态
   */
  status = '';
  /**
   * 图标
   */
  icon = () => {
    return '';
  };
  /**
   * 图标颜色
   */
  iconColor = '';
  /**
   * 弹出位置
   */
  placement = 'bottomRight';
  /**
   * 操作按钮
   */
  buttons = [];

  /**
   * 唯一标识
   */
  // key = 'updatable';

  /**
   * 播放声音
   */
  playAudio = false;

  /**
   * 时间
   */
  createdTime;
  /**
   * 展示关闭按钮
   */
  showClose = false;

  constructor({ content }) {
    const options = decode(content) ?? {};
    this.message = options.title;
    this.description = () =>
      h('div', {
        innerHTML: options.content,
      });
    this.content = options.content ?? this.content;
    this.closable = options.closable ?? this.closable;
    this.status = options.status ?? this.status;
    this.duration = options.duration ?? this.duration;
    // this.key = 'updatable';
    this.icon = options.icon ?? this.icon;
    this.iconColor = options.iconColor ?? this.iconColor;
    this.placement = options.placement ?? this.placement;
    this.buttons = options.buttons?.map?.((button) => new NoticeButton(button)) ?? [];
    this.createdTime = getDateDiff(formatTime(options.content));
    this.showClose = options.showClose ?? this.showClose;
    this.formatData = transDate(formatTime(options.content));
    this.playAudio = options.playAudio ? true : false;

    // 关闭特殊处理
    // if (!this.closable) {
    //   this.duration = 0;
    //   this.closeIcon = () => null;
    // }
  }
}
export function formatTime(content) {
  if (!content) return '';
  const formatContent = content.split('，');
  return formatContent[0];
}

//字符串转换为时间戳
export function getDateTimeStamp(dateStr) {
  return Date.parse(dateStr.replace(/-/gi, '/'));
}

export function getDateDiff(dateStr) {
  var publishTime = getDateTimeStamp(dateStr) / 1000,
    d_seconds,
    d_minutes,
    d_hours,
    d_days,
    timeNow = parseInt(new Date().getTime() / 1000),
    d,
    date = new Date(publishTime * 1000),
    Y = date.getFullYear(),
    M = date.getMonth() + 1,
    D = date.getDate(),
    H = date.getHours(),
    m = date.getMinutes(),
    s = date.getSeconds();
  //小于10的在前面补0
  if (M < 10) {
    M = '0' + M;
  }
  if (D < 10) {
    D = '0' + D;
  }
  if (H < 10) {
    H = '0' + H;
  }
  if (m < 10) {
    m = '0' + m;
  }
  if (s < 10) {
    s = '0' + s;
  }
  d = timeNow - publishTime;
  d_days = parseInt(d / 86400);
  d_hours = parseInt(d / 3600);
  d_minutes = parseInt(d / 60);
  d_seconds = parseInt(d);
  if (d_days > 0 && d_days < 3) {
    return d_days + '天前';
  } else if (d_days <= 0 && d_hours > 0) {
    return d_hours + '小时前';
  } else if (d_hours <= 0 && d_minutes > 0) {
    return d_minutes + '分钟前';
  } else if (d_seconds < 60) {
    if (d_seconds <= 0) {
      return '刚刚';
    } else {
      return d_seconds + '秒前';
    }
  } else if (d_days >= 3 && d_days < 30) {
    return M + '-' + D + '&nbsp;' + H + ':' + m;
  } else if (d_days >= 30) {
    return Y + '-' + M + '-' + D + '&nbsp;' + H + ':' + m;
  }
}

export function transDate(strtime) {
  const date = Date.parse(new Date(strtime.replace(/-/g, '/')));
  const tt = new Date(parseInt(strtime));
  const days = parseInt((new Date().getTime() - date) / 86400000);
  const today = new Date().getDate();
  const year = tt.getFullYear();
  const mouth = tt.getMonth() + 1;
  const day = tt.getDate();
  const time = tt.getHours() < 10 ? '0' + tt.getHours() : tt.getHours();
  const min = tt.getMinutes() < 10 ? '0' + tt.getMinutes() : tt.getMinutes();
  let result;
  const offset = Math.abs(today - day);
  if (days < 4 && offset < 4) {
    if (offset === 0) {
      result = '今天' + time + ':' + min;
    } else if (offset === 1) {
      result = '昨天' + time + ':' + min;
    } else if (offset === 2) {
      result = '前天' + time + ':' + min;
    }
  } else {
    result = year + '-' + mouth + '-' + day + ' ' + time + ':' + min;
  }
  return result;
}
/**
 * 操作按钮
 */
class NoticeButton {
  text;
  icon;
  iconColor;
  bgColor;
  borderColor;
  fontColor;

  /**
   * 脚本
   */
  script;
  /**
   * 脚本中的参数，可以通过EventOptions获取
   */
  params = {};

  constructor(options) {
    Object.assign(this, options);
  }
}

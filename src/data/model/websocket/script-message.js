import { isString, decode } from '@jecloud/utils';
/**
 * 操作按钮
 */
export default class ScriptMessage {
  /**
   * 脚本
   */
  script;
  /**
   * 脚本中的参数，可以通过EventOptions获取
   */
  params = {};

  constructor({ content, busType }) {
    content = decode(content) ?? content;
    if (isString(content)) {
      this.script = content;
    } else {
      this.script = content.script;
      this.params = content.params ?? this.params;
    }
    this.params = { busType, ...this.params };
  }
}

export default class Base {
  constructor() {}
  /**
   * 模型数据
   *
   * @readonly
   * @memberof Base
   */
  get isModel() {
    return true;
  }
}

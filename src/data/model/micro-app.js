import { MicroActiveType } from '../enum/micro';
import { isNotEmpty } from '@jecloud/utils';

/**
 * 微应用
 */
export default class MicroApp {
  constructor(options) {
    this.name = options.MICROAPP_CODE;
    this.title = options.MICROAPP_NAME;
    this.entry = options.MICROAPP_ENTRY;
    this.activeRoute = options.MICROAPP_ACTIVEROUTE;
    this.activeType = options.MICROAPP_ACTIVETYPE ?? MicroActiveType.MENU;
    this.menuType = options.MICROAPP_MENUTYPE_CODE; // 菜单类型
  }

  /**
   * 获取入口地址
   * @returns
   */
  getEntryUrl() {
    if (this.entry.startsWith('http')) {
      return this.entry;
    }
    // 追加系统地址
    let publicPath = window.location.pathname;
    if (publicPath.endsWith('/')) {
      publicPath = publicPath.substring(0, publicPath.length - 1);
    }
    return publicPath + this.entry;
  }

  /**
   * 解析激活路由地址
   * @param {*} querys
   * @returns
   */
  parseActiveRoute(querys) {
    let activeRoute = this.activeRoute || '';
    if (activeRoute && isNotEmpty(querys)) {
      activeRoute = activeRoute
        .split('/')
        .map((path) => {
          if (path.startsWith(':')) {
            return querys[path.substring(1)] || path;
          } else {
            return path;
          }
        })
        .join('/');
    }

    console.log(this.getEntryUrl() + activeRoute);
    return activeRoute;
  }
}

import { LayoutMenuType, MenuType } from '@/data/enum/menu';
import Base from '../base';

/**
 * 顶部菜单
 *
 * @export
 * @class TopMenu
 */
export default class TopMenu extends Base {
  /**
   * 菜单类型
   *
   * @memberof TopMenu
   */
  menuType = LayoutMenuType.TOP;

  constructor(options) {
    super();
    this.id = options.JE_RBAC_HEADMENU_ID;
    this.code = options.HEADMENU_CODE;
    this.text = options.HEADMENU_NAME;
    this.icon = options.HEADMENU_ICON || 'fal fa-circle';
    this.iconColor = options.HOME_ZTTBYS;
    this.type = options.HEADMENU_TYPE_CODE;
    this.logo = options.HEADMENU_SYSTEM_LOGO;
    this.configInfo = options.HEADMENU_CONFIGINFO;
    this.initMenuId = options.HEADMENU_INITMENU_ID;
    this.leaf = this.type !== MenuType.MENU;
    this.relations = (options.relations ?? []).map((item) => ({
      id: item.JE_CORE_MENU_ID,
      text: item.MENU_MENUNAME,
    }));
  }
}
